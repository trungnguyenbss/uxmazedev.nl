jQuery(function($){

    if (typeof(Hellothemes_Priceslider) == "undefined") return;

    function reload(url) {
        $.blockUI({ message:null, overlayCSS: {opacity:0.16, zIndex:99999} });
        $('.col-main').first().load(url, function(){
            $.unblockUI();
            _resizeLimit = {};
            $(window).resize();
            /*$('.col-main .category-products').scrollToMe();*/
        });
    }

    //toolbar sort and per page
    $('.toolbar select').live('change', function(){
        reload($(this).val());
        return false;
    });
    //toolbar
    $('.toolbar .toolbar-dropdown a, .toolbar .sort-order a').live('click', function(){
        reload($(this).attr('href'));
        return false;
    });
    //toolbar view mode
    $('.view-mode a').live('click', function(){
        var slidervalue = $("#slider").slider('values');
        var ids = $("#category").val();
        var correctbaseurl = $('#slider-baseurl').val() + 'priceslider/slider/view?min=' + slidervalue[0] + '&max=' + slidervalue[1] + '&id=' + ids + '&mode=' + $(this).attr('class');
        reload(correctbaseurl);
        return false;
    });
    //pager
    $('.pager .pages a').live('click', function(){
        reload($(this).attr('href'));
        return false;
    });

    //slider
    function reloadProducts() {
        var ids = $("#category").val();
        var slidervalue = jQuery("#slider").slider('values');
        var correctbaseurl = $('#slider-baseurl').val() + 'priceslider/slider/view?min=' + slidervalue[0] + '&max=' + slidervalue[1] + '&id=' + ids;
        if ( Hellothemes_Priceslider.request_params != '' ) {
            correctbaseurl += '&' + Hellothemes_Priceslider.request_params;
        }
        reload(correctbaseurl);
    }
    $("#slider").slider({range:true,
        min:0,
        max:$("#max-price").val(),
        values:[Hellothemes_Priceslider.slider_min, Hellothemes_Priceslider.slider_max],
        slide:function (event, ui) {
            $("#slider-min").html(Hellothemes_Priceslider.currency+ui.values[0]);
            $("#slider-max").html(Hellothemes_Priceslider.currency+ui.values[1]);
            },
        create:function (event, ui) { reloadProducts(); },
        stop:function (event, ui) {
            $.cookie("hellothemes_priceslider_min_"+Hellothemes_Priceslider.category_id, ui.values[0], { path: '/' });
            $.cookie("hellothemes_priceslider_max_"+Hellothemes_Priceslider.category_id, ui.values[1], { path: '/' });
            reloadProducts();
        }
    });
});