//SCRIPTS NECESARIOS PARA MODULO HELLOREVOLUTION

//actual width
;(function(a){a.fn.extend({actual:function(b,k){var c,d,h,g,f,j,e,i;if(!this[b]){throw'$.actual => The jQuery method "'+b+'" you called does not exist';}h=a.extend({absolute:false,clone:false,includeMargin:undefined},k);d=this;if(h.clone===true){e=function(){d=d.filter(":first").clone().css({position:"absolute",top:-1000}).appendTo("body");};i=function(){d.remove();};}else{e=function(){c=d.parents().andSelf().filter(":hidden");g=h.absolute===true?{position:"absolute",visibility:"hidden",display:"block"}:{visibility:"hidden",display:"block"};f=[];c.each(function(){var m={},l;for(l in g){m[l]=this.style[l];this.style[l]=g[l];}f.push(m);});};i=function(){c.each(function(m){var n=f[m],l;for(l in g){this.style[l]=n[l];}});};}e();j=/(outer)/g.test(b)?d[b](h.includeMargin):d[b]();i();return j;}});})(jQuery);

var sw, sh, scroll_critical,
    breakpoint = 959,
    mobile = false,
    resizeLimits = [0,479,767,959,1199,9999],
    _resizeLimit = {};

isResize = function(limitName){
    var current, w = jQuery(window).width();
    for( i=0; i<resizeLimits.length; i++ ){
        if (w > resizeLimits[i]) {
            current = i;
        } else {
            break;
        }
    }
    if ( _resizeLimit[limitName] === undefined || current != _resizeLimit[limitName] ) {
        _resizeLimit[limitName] = current;
        return true;
    }
    return false;
}

jQuery(function($){

    //fix grid items height
    function fixGridHeight() {
        $('.products-grid').each(function(){
            var items_in_row = Math.floor($(this).width() / $('li.item', this).outerWidth(true));
            var height = [], row = 0;
            $('li.item', this).each(function(i,v){
                $('div.product-info', this).css('height', 'auto');
                var h = $('div.product-info', this).height();
                if ( !height[row] ) {
                    height[row] = h;
                } else if ( height[row] && h > height[row] ) {
                    height[row] = h;
                }
                if ( (i+1)/items_in_row == 1 ) row++;

            });
            row = 0;
            $('li.item', this).each(function(i,v){
                $('div.product-info', this).height( height[row] );
                if ( (i+1)/items_in_row == 1 ) row++;
            });
        });
    }
    fixGridHeight();

    var $flexslider = $("#flexslider"),
        $flexslides = $flexslider.find('ul.slides').children('li');

    if ( $flexslider.length ) {
        $(window).load(function(){

            var timeline = {
                    width: 0,
                    interval: CONFIG_SLIDESHOW.slideshowSpeed
                },
                slideshowHover = false,
                slideshowPause = false;

            vericalCenterSlideContent = function($slide) {
                var $content = $('div.content', $slide);
                var $contentH = $content.actual('height')
                    + parseInt( $content.css('marginTop') )
                    + parseInt( $content.css('marginBottom') );
                if ( $slide.height() > $contentH ) {
                    $content.css('marginTop', Math.floor( ($slide.height() - $contentH)/2 + 30 ) + 'px');
                }
            }

            setSlideHeight = function() {
                //update slides to include cloned li
                $flexslides = $("#flexslider").find('ul.slides').children('li');
                if (_resizeLimit['slideshow'] <= 1 && Hellothemes.responsive ) {
                    //iphone resolution ( <= 767 ). hide content and show small image
                    $('div.content', $flexslides).hide();
                    $('img.small_image', $flexslides).show();
                    var maxSlideHeight = null;
                    $flexslides.each(function(i,v){
                        if ( $('img.small_image', this).length ) {
                            $(this).css('background-image', 'none');
                            $(this).height($('img.small_image', this).height());
                            maxSlideHeight = Math.max(maxSlideHeight, $(this).height());
                        }
                    });
                    //auto height - by tallest slide
                    $flexslides.height(maxSlideHeight);
                } else {
                    $('img.small_image', $flexslides).hide();
                    $('div.content', $flexslides).show();
                    //restore original content margin top
                    $('div.content', $flexslides).css('marginTop', '30px');
                    //restore bg image
                    $flexslides.each(function(i,v){
                        $(this).css('background-image', $(this).attr('data-bg'));
                    });

                    if ( CONFIG_SLIDESHOW.height != 'auto' ) {
                        $flexslides.height(CONFIG_SLIDESHOW.height);
                    } else {
                        var maxSlideHeight = null;
                        //set slide height according to height of content and image
                        $flexslides.each(function(i,v){
                            var $imgH = $(this).attr('data-img-height');
                            //count content height
                            var $contentH = $('div.content', this).actual('height') + parseInt($('div.content', this).css('marginTop')) + parseInt($('div.content', this).css('marginBottom'));
                            $(this).height(Math.max($imgH, $contentH)+'px');
                            maxSlideHeight = Math.max(maxSlideHeight, $(this).height());
                        });

                        if ( CONFIG_SLIDESHOW.smoothHeight ) {
                            //smooth height
                        } else {
                            //auto height - by tallest slide
                            $flexslides.height(maxSlideHeight);
                        }
                    }
                    //adjust content vertical center
                    $flexslides.each(function(i,v){
                        vericalCenterSlideContent( $(this) );
                    });
                }
            }

            //backup original images for slides
            $flexslides.each(function(i,v){
                $(this).attr('data-bg', $(this).css('background-image'));
            });

            slideshowResize = function() {
                timeline.width = $flexslider.width();
                var resize = isResize('slideshow');
                if (resize || _resizeLimit['slideshow'] <= 1) {
                    setSlideHeight();
                }
            }
            slideshowResize();

            $(window).resize(function(){
                var interval = (timeline.width - $('#slide-timeline').width() ) / ( timeline.width / timeline.interval );
                runTimeline(interval);
                slideshowResize();
            });

            runTimeline = function( interval ) {
                if ( slideshowPause
                    || interval == 0
                    || CONFIG_SLIDESHOW.slideshow == false
                    || CONFIG_SLIDESHOW.timeline == false
                    || $flexslides.length < 2) {return;}
                $('#slide-timeline')
                    .show()
                    .animate(
                    {width: timeline.width + 'px'},
                    interval,
                    'linear',
                    function(){
                        $(this).hide().width(0);
                        $('#flexslider').flexslider("next");
                    }
                );
            }

            $flexslider.on({
                mouseenter: function () {
                    slideshowPause = true;
                    slideshowHover = true;
                    $('#slide-timeline').stop(true);
                },
                mouseleave: function () {
                    slideshowPause = false;
                    slideshowHover = false;
                    var interval = (timeline.width - $('#slide-timeline').width() ) / ( timeline.width / timeline.interval );
                    runTimeline(interval);
                },
                touchstart: function () {
                    $('#slide-timeline').stop(true);
                }
            });

            var defaults = {
                slideshow: ( CONFIG_SLIDESHOW.slideshow && CONFIG_SLIDESHOW.timeline == false ? true : false),
                initDelay:200,
                start: function(slider){
                    setSlideHeight();
                    //line up direction nav
                    if (CONFIG_SLIDESHOW.smoothHeight) {
                        $('.flex-direction-nav a', slider).css('marginTop', (-$('li.flex-active-slide', slider).height()/2 -40) );
                    } else {
                        $('.flex-direction-nav a', slider).css('marginTop', (-$('.flexslider').height()/2 -40) );
                    }
                    runTimeline(timeline.interval);
                },
                before: function(slider){
                    $('#slide-timeline').hide().width(0);
                    $('.flex-direction-nav a', slider).hide();
                },
                after: function(slider){
                    if ( !slideshowHover ) {
                        slideshowPause = false;
                    }
                    if (CONFIG_SLIDESHOW.smoothHeight) {
                        $('.flex-direction-nav a', slider).css('marginTop', (-$('li.flex-active-slide', slider).height()/2 -40));
                    }
                    $('.flex-direction-nav a', slider).show();
                    $('#slide-timeline').stop(true);
                    $('#slide-timeline').hide().width(0);
                    runTimeline(timeline.interval);
                }
            }
            vars = $.extend({}, CONFIG_SLIDESHOW, defaults);

            if ( $('.col-main .homepage-banners').length ) {
                $('.slider').animate({paddingBottom: '52px'}, 200);
            }
            if ( $('.col-main .home-right').length ) {
                $('.slider').animate({paddingBottom: '20px'}, 200);
            }

            $flexslider.flexslider(vars);
        });
    } else {
        if ( $('.col-main .homepage-banners').length ) {
            $('.col-main .homepage-banners').css('padding-top','40px');
        }
    }

    $(window).load(function(){
        if ( $(".block-slideshow .block-slider").length ) {
            $(".block-slideshow .block-slider")
                .flexslider({
                    animation: "slide",
                    slideshow: true,
                    useCSS: false,
                    touch: true,
                    video: false,
                    animationLoop: true,
                    mousewheel: false,
                    keyboard:false,
                    smoothHeight: false,
                    slideshowSpeed: 7000,
                    animationSpeed: 600,
                    pauseOnAction: true,
                    pauseOnHover: true,
                    controlNav: true,
                    directionNav: false,
                    start: function(s){ $('.col-left').masonry('reload'); }
                });
        }
        if ( $(".block-login .block-slider").length) {
            $(".block-login .block-slider")
                .flexslider({
                    animation: "slide",
                    slideshow: false,
                    useCSS: false,
                    touch: true,
                    video: false,
                    keyboard:false,
                    animationLoop: false,
                    smoothHeight: false,
                    animationSpeed: 600,
                    controlNav: false,
                    directionNav: false
                });
            $('#forgot-password').click(function(){ $(".block-login .block-slider").flexslider("next"); return false; });
            $('#back-login').click(function(){ $(".block-login .block-slider").flexslider("prev"); return false; });
            if ( $('body').hasClass('customer-account-forgotpassword') ) {
                $('#forgot-password').click();
            }
        }
    });

    if (typeof CONFIG_REVOLUTION !== 'undefined') {
        if ($.fn.cssOriginal!=undefined)   // CHECK IF fn.css already extended
            $.fn.css = $.fn.cssOriginal;
        $('.fullwidthbanner').revolution(CONFIG_REVOLUTION);
    }

});