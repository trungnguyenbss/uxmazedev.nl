var pixelRatio = !!window.devicePixelRatio ? window.devicePixelRatio : 1;
jQuery(window).on("load", function() {

    //ipad and iphone fix
    if((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i)) || (navigator.userAgent.match(/iPad/i))) {
        jQuery("#hellothemesNav li a").on({
            click: function () {
                if ( !jQuery(this).hasClass('touched') && jQuery(this).siblings('div') ) {
                    jQuery('#hellothemesNav a').removeClass('touched');
                    jQuery(this).parents('li').children('a').addClass('touched');
                    jQuery(this).addClass('touched');
                    return false;
                }
            }
        });
        jQuery("#nav li a").on({
            click: function () {
                if ( !jQuery(this).hasClass('touched') && jQuery(this).siblings('ul') ) {
                    jQuery('#nav a').removeClass('touched');
                    jQuery(this).parents('li').children('a').addClass('touched');
                    jQuery(this).addClass('touched');
                    return false;
                }
            }
        });
        jQuery('.header-switch, .toolbar-switch').on({
            click: function (e) {
                jQuery(this).addClass('over');
                return false;
            }
        });

    }
	
	jQuery('#narrow-by-list dt').click(function(){
		jQuery(this).toggleClass('active');
		jQuery(this).next('dd').slideToggle('medium');
	});
	
	jQuery("#store-content ul li").mouseenter(function() {
		jQuery('.hover-store', this).css('opacity' , '1');
	}).mouseleave(function() {
		jQuery('.hover-store', this).css('opacity' , '0');
	});

    jQuery(window).resize().scroll();
	
	jQuery('#tabs').tabs();
	
	jQuery('.products-grid, #color-content, .block-related, #hellothemes_carousel').jcarousel({
		horizontal: true,
        scroll: 1,
		itemFallbackDimension: 300
	});
	
	jQuery('#multizoom1').addimagezoom({ // multi-zoom: options same as for previous Featured Image Zoomer's addimagezoom unless noted as '- new'
		descArea: '#description', // description selector (optional - but required if descriptions are used) - new
		speed: 1500, // duration of fade in for new zoomable images (in milliseconds, optional) - new
		descpos: true, // if set to true - description position follows image position at a set distance, defaults to false (optional) - new
		imagevertcenter: true, // zoomable image centers vertically in its container (optional) - new
		magvertcenter: true, // magnified area centers vertically in relation to the zoomable image (optional) - new
		zoomrange: [3, 10],
		magnifiersize: [500,500],
		magnifierpos: 'right',
		cursorshadecolor: '#fdffd5',
		cursorshade: true //<-- No comma after last option!
	});
	
	jQuery('#color-content').jcarousel({
		horizontal: true,
        scroll: 1,
		wrap: 'circular',
		itemFallbackDimension: 300
	})
	
	//Cart add/remove buttons
	jQuery("#cart_add").click(function(){
		cart_qty=parseInt(jQuery("#qty").val());
		jQuery("#qty").val(cart_qty+1);
		return false;
	});
	jQuery("#cart_remove").click(function(){
		cart_qty=parseInt(jQuery("#qty").val());
		if (cart_qty>0)
			jQuery("#qty").val(cart_qty-1);
		return false;
	});

	jQuery(".sticker-top-right").mouseenter(function() {
		 jQuery(this).children('.specialto').show();
	}).mouseleave(function() {
		jQuery(this).children('.specialto').hide();
	});
	
	
	jQuery(".products-grid li, #upsell-product-table li").each(function() {
		var view = jQuery(this).find(".quick-view span");
		jQuery(".catalog-image", this).hover(function() {
			view.css("display","block").animate({
				opacity: 1
			}, 200);
		}, function() {
			view.animate({
				opacity: 0
			}, 200, function() {
				jQuery(this).css("display","none");
			});
		});
		view.hide();
	});
	
	//jQuery(".switcher .store a, .switcher .settings a, #setting-content ul li a, .switcher .codeqr a").fancybox();

});

jQuery.fn.extend({
    scrollToMe: function () {
        var x = jQuery(this).offset().top - 100;
        jQuery('html,body').animate({scrollTop: x}, 500);
    }
});

jQuery(function($){

    if (Hellothemes.totop) {
        $().UItoTop({scrollSpeed:400});
    }

    //.page-title-bg
    var page_title_height = '';
    if ( $('.main-container .page-title').length ) {
        var $p = $('<div class="page-title-bg" />').css({height: $('.main-container .page-title').height()+60 });
        $('.main-container').prepend($p);
        page_title_height = 'title';
    } else if ( $('.main-container .breadcrumbs:visible').length ) {
        var $p = $('<div class="page-title-bg" />').css({height: $('.main-container .breadcrumbs').height() });
        $('.main-container').prepend($p);
        page_title_height = 'breadcrumbs';
        if ( $('.product-category-title').length ) {
            $('.main-container .page-title-bg').css({height: $('.main-container .breadcrumbs').height() + $('.product-category-title').outerHeight() });
            page_title_height = 'breadcrumbs_category';
        }
    }

    $(window).resize(function(){

        if ( !isResize('page_title') ) return;

        if ( page_title_height == '' ) return;
        var h = 0;
        switch (page_title_height) {
            case 'title' :
                h = $('.main-container .page-title').height() + 60;
                break;
            case 'breadcrumbs' :
                h = $('.main-container .breadcrumbs').height();
                break;
            case 'breadcrumbs_category' :
                h = $('.main-container .breadcrumbs').height() + $('.product-category-title').outerHeight();
                break;
        }
        $('.main-container .page-title-bg').css({height: h });
    });

    $(window).resize(function(){
        sw = $(window).width();
        sh = $(window).height();
        mobile = (sw > breakpoint) ? false : true;
        if ( !Hellothemes.responsive ) {
            mobile = false;
        }

        //menu_transform
        if (!($("header").hasClass("fixed"))) $(".header-wrapper").height($("header").height());
        scroll_critical = parseInt($(".header-container").height());

        //header_transform();
        if ( !isResize('grid_header') ) return;
        fixGridHeight();
	
    });
	
	var hgt = $('.flex-viewport').height();
	$('.img-right-slide').height(hgt+'px');
	
	//cart dropdown
	var config = {
	     over: function(){
            if (mobile) return;
            $('.cart-top-container .details').animate({opacity:1, height:'toggle'}, 200);
        },
	     timeout: 200, // number = milliseconds delay before onMouseOut
	     out: function(){
            if (mobile) return;
            $('.cart-top-container .details').animate({opacity:0, height:'toggle'}, 200);
         }
	};
	$("div.cart-top-container").hoverIntent( config );

    $('#hellothemesNav li, .custom-block').hover(
        function(){
            $(this).addClass('over');
            var div = $(this).children('div');
            div.addClass('shown-sub');
            if ( div.actual('width') + $(this).offset().left > $(document).width()  ) {
                div.css('left', -($(this).offset().left + div.actual('width') + 5 - $(document).width())+'px' );
            } else {
                div.css('left', '0px');
            }
        },
        function(){
            $(this).removeClass('over');
            $(this).children('div').removeClass('shown-sub').css('left', '-10000px');
        }
    );

	//fix grid items height
    function fixGridHeight() {
        $('.products-grid').each(function(){
            var items_in_row = Math.floor($(this).width() / $('li.item', this).width());
            var height = [], row = 0;
            $('li.item', this).each(function(i,v){
                var h = $(this).height();
                if ( !height[row] ) {
                    height[row] = h;
                } else if ( height[row] && h > height[row] ) {
                    height[row] = h;
                }
                if ( (i+1)/items_in_row == 1 ) row++;

            });
            row = 0;
            $('li.item', this).each(function(i,v){
                $(this).height( height[row] );
                if ( (i+1)/items_in_row == 1 ) row++;
            });
        });
    }
    fixGridHeight();

    var config = {
        over: function(){
            if (mobile) return;
            if ($(this).hasClass('.toolbar-dropdown')){
                $(this).parent().addClass('over');
                $('.toolbar-dropdown').css({width: $(this).parent().width()+50});
            } else {
                $(this).addClass('over');
                $('.toolbar-dropdown', this).css({width: $(this).width()+50});
            }

            $('.toolbar-dropdown', this).animate({opacity:1, height:'toggle'}, 100);
        },
        timeout: 0, // number = milliseconds delay before onMouseOut
        out: function(){
            if (mobile) return;
            var that = this;
            $('.toolbar-dropdown', this).animate({opacity:0, height:'toggle'}, 100, function(){
                if ($(this).hasClass('.toolbar-dropdown')){
                    $(that).parent().removeClass('over');
                } else {
                    $(that).removeClass('over');
                }
            });
        }
    };
    $('.toolbar-switch, .toolbar-switch .toolbar-dropdown').hoverIntent( config );

    var config = {
        over: function(){
            $('.back_img', this).css('opacity',0).show().animate({opacity:1}, 200);
        },
        timeout: 100, // number = milliseconds delay before onMouseOut
        out: function(){
            $('.back_img', this).animate({opacity:0}, 200);
        }
    };
    $('.products-list .product-image').hoverIntent( config );

    $('.products-grid .item').live({
        mouseenter: function(){
            if (mobile) return;
            $('.hover .price-box', this).css({
                'opacity':0
            });
            if (Hellothemes.price_circle) {
                if ( !$(this).hasClass('calc-price-box') ) {
                    var padding = Math.floor( ($('.hover .price-box', this).actual('width') - $('.hover .price-box', this).actual('height'))/2 + 15 );
                    $('.hover .price-box', this).css({
                        'padding':padding+'px 15px',
                        'margin':(-(25+padding*2+$('.hover .price-box', this).actual('height')))+'px 0 0 0',
                    });
                    $(this).addClass('calc-price-box');
                }
                var that = this;
                $('.hover', this).show(0, function(){ $('.hover .price-box', that).animate({opacity:1}, 600) } );
            } else {
                $('.hover', this).show();
            }

            $(this).addClass('no-shadow');

        },
        mouseleave: function(){
            if (mobile) return;
            $('.hover', this).hide();
            $(this).removeClass('no-shadow');
        }
    });

    
   $(window).load(function(){
        setTimeout(function(){ if ($('.col-left').length) $('.col-left').masonry({itemSelector : '.block', isResizable:true}); }, 600);
    });

    if ( Hellothemes.anystretch_bg != '' ) {
        jQuery('.main-container').anystretch( Hellothemes.anystretch_bg );
    }

    if ( $('body').hasClass('customer-account-login') || $('body').hasClass('customer-account-forgotpassword') ) {
        function positionFooter() {
            if (mobile) return;
            if (!$("#sticky-footer-push").length) {
                $(".footer-container").before('<div id="sticky-footer-push"></div>');
            }
            var docHeight = $(document.body).height() - $("#sticky-footer-push").height();
            if(docHeight < $(window).height()){
                var diff = $(window).height() - docHeight - 5;
                $("#sticky-footer-push").height(diff);
            }
        }
        $(window).scroll(positionFooter).resize(positionFooter).load(positionFooter);
    }
});

var $jQ = jQuery.noConflict();

$jQ(document).ready(function(){	

	$jQ(".category span").click(function() {
		var open = $jQ(this).parent('.category').attr('lang');
		$jQ(".subcategory_" + open).slideToggle('medium');
		$jQ(".subcategory_" + open).parent().prev().toggleClass('openn');
	}); 
	$jQ("#left-nav li.category, #left-nav li.cate").mouseenter(function() {
		$jQ(this).addClass('over');
	}).mouseleave(function() {
		$jQ(this).removeClass('over');
	}); 
	
	$jQ('#left-nav .cate.active').parents('#left-nav').find('.category.active a').css({'background': 'none repeat scroll 0 -2px transparent', 'color': '#999999'});
	$jQ('#left-nav .cat.active').parents('#left-nav').find('.category.active a').css({'background': 'none repeat scroll 0 -2px transparent', 'color': '#999999'})
	$jQ('#left-nav .cat.active').parents('#left-nav').find('.cate.active a').css({'background': 'none repeat scroll 0 -2px transparent', 'color': '#666666'})
	
	$jQ('.header-switch span.current').click(function(){
		$jQ('.header-dropdown').slideToggle('medium');
	});	
	
	var angle = 180;    
	$jQ('span.current em, .currency-switch em').click(function() {
	   //$(this).toggleClass('active');
		$jQ(this).css ({
			'-webkit-transform': 'rotate(' + angle + 'deg)',
			'-moz-transform': 'rotate(' + angle + 'deg)',
			'-o-transform': 'rotate(' + angle + 'deg)',
			'-ms-transform': 'rotate(' + angle + 'deg)'
		});
		angle+=180;
	});	
		
	$jQ('.currency-switch').click(function(){
		$jQ('.ulSelect2').slideToggle('medium');
	});

	$jQ('.cart-top-container span.open').click(function(){
		$jQ('.details').slideToggle('medium');
	});
	
	var select = $jQ('.currency-switch li.selected').html();
	$jQ('.selectSwitch2').append(select);
	$jQ(".selectSwitch2").click(function() {
		$jQ('.selectSwitch2').html('');
		var select = $jQ('.currency-switch li.selected').html();
		$jQ('.selectSwitch2').append(select);
		$jQ('.ulSelect2').slideToggle('medium');
	});

});