<?php
define('MAGENTO_ROOT', (dirname(__FILE__).'/../../../../..'));
$mageFilename = MAGENTO_ROOT . '/app/Mage.php';
require_once $mageFilename;

umask(0);

if ( empty($_GET['store']) ) {
    $_GET['store'] = '';
}
Mage::app( $_GET['store'] );
$config = Mage::getStoreConfig('hellothemessettings', $_GET['store']);

$color_helper = Mage::helper('hellothemessettings/color');

            foreach ($config['appearance'] as $config_option => $value) {
                if (!empty($current_scheme[$config_option])) {
                    $config['appearance'][$config_option] = $current_scheme[$config_option];
                }
            }
            foreach ($config['header'] as $config_option => $value) {
                if (!empty($current_scheme[$config_option])) {
                    $config['header'][$config_option] = $current_scheme[$config_option];
                }
            }
            foreach ($config['fonts'] as $config_option => $value) {
                if (!empty($current_scheme[$config_option])) {
                    $config['fonts'][$config_option] = $current_scheme[$config_option];
                }
            }
            foreach ($config['products'] as $config_option => $value) {
                if (!empty($current_scheme[$config_option])) {
                    $config['products'][$config_option] = $current_scheme[$config_option];
                }
            }
            foreach ($config['footer'] as $config_option => $value) {
                if (!empty($current_scheme[$config_option])) {
                    $config['footer'][$config_option] = $current_scheme[$config_option];
                }
            }

header("Content-type: text/css; charset: UTF-8");
?>
<?php if ( $config['fonts']['enable_font'] ) : ?>
/**~~ Theme Font ~~**/
.std h1, .std h2, .std h3, .std h4,
.page-title h1, .page-title h2,
.cart-top-container .details .cart-top-title,
.search-top-container .search-form .search-top-title,
.footer-info h4,
nav .nav-top-title, .nav-container .nav-top-title,
#hellothemesNav>li>a,
#hellothemesNav li.custom-block div.sub-wrapper strong,
#nav>li>a,
#nav li.custom-block ul li strong,
.homepage-banners a .content strong,
.slider-container h3,
.slider-container .jcarousel-list h3,
.category-description h1,
.category-description strong,
.products-grid h3,
.products-list .product-name a,
.cart .cart-collaterals h2,
#shopping-cart-totals-table strong,
#product-customer-reviews .review-title,
.add-review h3.title,
#customer-reviews dt a,
#customer-reviews .form-add h2,
.top-opc li .number,
.opc .step-title,
.opc h3,
.block .block-title strong,
.cms-index-index .block .block-title strong,
.block-poll .question,
.block-layered-nav dt,
.product-category-title,
.product-view h1,
.product-view h2,
.product-view .box-tags h3,
.product-view .product-additional .block .block-title strong,
.box-up-sell2 h3,
.box-up-sell2 .jcarousel-list h3,
.flexslider .slides .content strong,
.data-table td.td-name h2,
.block-slider .slides > li strong,
.button_slide,
.cms-index-index .block-slider .slides > li strong, 
.information  h2, #nav ul li a {font-family:"<?php echo $config['fonts']['font']; ?>"}
<?php endif; ?>
<?php if ( !empty($config['appearance']['color']) ) : ?>
/**~~ Theme Color ~~**/
.jcarousel-prev-horizontal.jcarousel-prev-disabled-horizontal, .jcarousel-next-horizontal.jcarousel-next-disabled-horizontal,
.cart-shipping button:hover span, .cart-coupon button:hover span,
.btn-remove:hover, .btn-edit:hover,
.cart-top-container .details-border,
.cart-top-container .details .cart-top-title a span.icon,
.search-top,
.search-form-border,
.search-top-container .search-form .search-top-title span.icon,
.footer-info ul.twitterList li span.tweet-icon,
.footer-info ul.social li a:hover,
.footer-info .block-control:hover,
.footer-info .block-control-hide:hover,
.products-grid .hover .price-box,
.products-grid .hover .button-container button.button span span em,
.products-list .button-container .btn-add-cart:hover,
.data-table .btn-remove2,
.data-table .btn-edit2,
.top-opc li.allow:hover .number,
.jcarousel-prev-horizontal:hover,
.jcarousel-next-horizontal:hover,
.product-view .box-up-sell .jcarousel-next-horizontal:hover,
.product-view .box-up-sell .jcarousel-prev-horizontal:hover,
#fancybox-close:hover,
.ui-widget-header,
.ui-state-active a, .ui-state-active a:link, .ui-state-active a:visited,
.ui-tabs .ui-tabs-nav li.ui-tabs-selected a,
.jcarousel-next-horizontal, .jcarousel-prev-horizontal,
.cart-top-container .btn-checkout,
#search_mini_form button, 
.footer-subscribe button.button,
.topcategory .category-description,
.block-compare .block-content .actions a:before,
#narrow-by-list dt em, #nav .content-block img, .slider-banner .banner-block span img, #hellothemesNav .content-block img, 
.cart-top > a:hover, .cart-top-container .summary, .cms-index-index .home, 
.block-poll input[type="radio"]:not(old) + label > span, #nav > li:hover, 
#nav > li.over, #nav > li.active, #hellothemesNav > li.active, 
.tp-bullets.simplebullets.round .bullet:hover, .tp-bullets.simplebullets.round .bullet.selected{background-color:<?php echo $config['appearance']['color']; ?>}

button.button:hover, #search_mini_form .form-search button:hover, a.btn-cart:hover, a.link-cart:hover {background: rgba(<?php echo $color_helper->hex2RGB($config['appearance']['color'], 1); ?>, 0.85);}

.ui-tabs .ui-tabs-nav li.ui-tabs-selected a:before {border-left: 19px solid <?php echo $config['appearance']['color']; ?>;}
.slider-banner .banner-block a {color: <?php echo $config['appearance']['color']; ?>;}
.slider-banner .banner-block a:hover {color: <?php echo $config['appearance']['hover_icon']; ?>;}
.checkout-progress li.active {border-top-color: <?php echo $config['appearance']['color']; ?>; color:<?php echo $config['appearance']['color']; ?>;}

.footer-info .information ul li:before,
.toolbar-dropdown ul li a:hover, .toolbar-dropdown ul li.selected a,
.opc h3, .opc h4,
#checkout-step-login .block-checkout-register ul.ul li:before, 
.product-view .availability, .product-tabs li.active a,
.footer-info .twitterWidget a, .products-list .ratings a:hover, 
.block .block-title strong {color:<?php echo $config['appearance']['color']; ?>}

.cart-top-container .details-border:before{border-color: transparent transparent <?php echo $config['appearance']['color']; ?> transparent;}
.search-form-border:before{border-color: transparent transparent <?php echo $config['appearance']['color']; ?> transparent;}
.cart .cart-collaterals .cart-block,
.opc .step,
.block,
.cms-index-index .block,
.block-login,
.cart .cart-collaterals .cart-block {border-top-color:<?php echo $config['appearance']['color']; ?>;}

/** links hover color **/
.header-container .links li a:hover,
.block .block-content a:hover, .block .block-content li a:hover, .block .block-content li.item a:hover,
.cms-index-index .block .block-content a:hover, .cms-index-index .block .block-content li a:hover, .cms-index-index .block .block-content li.item a:hover,
.block-layered-nav .block-content dd li a:hover,
.product-view .product-shop .no-rating a:hover, .product-view .product-shop .ratings a:hover,
.product-view .box-up-sell .product-name:hover,.data-table td a:hover, .product-view .box-tags ul.product-tags li a:hover{color:<?php echo $config['appearance']['color']; ?>}
<?php endif; ?>

<?php if ( !empty($config['appearance']['title_color']) ) : ?>
h1, h2, h3,
.std h1, .std h2, .std h3, .std h4, .page-title h1, .page-title h2, .page-head-alt h3, .block .block-title, .cms-index-index .block .block-title, .block-login .block-title,
.product-view .product-additional .block .block-title, .footer-info h4, #checkout-review-table h3, .product-category-title, .product-view h1, .product-view h2,
#shopping-cart-totals-table strong, .products-list .product-name a, .products-grid .product-name a, .product-view .prod-content h1, #upsell-product-table .product-name{color:<?php echo $config['appearance']['title_color']; ?>}
<?php endif; ?>

<?php if ( !empty($config['header']['menu_text_color']) ) : ?>
#hellothemesNav > li > a, #nav > li > a {color:<?php echo $config['header']['menu_text_color']; ?>}
<?php endif; ?>


<?php if ( !empty($config['appearance']['button_color']) ) : ?>
.products-grid .btn-cart, .products-list .btn-cart, button.button, .link-cart{background:<?php echo $config['appearance']['button_color']; ?>}
<?php endif; ?>

#toTop, #search_mini_form .form-search button, .top-opc li.allow .number{background-color:<?php echo $config['appearance']['button_color']; ?>}

<?php if ( !empty($config['appearance']['content_bg']) ) : ?>
.main-container {background-color:<?php echo $config['appearance']['content_bg']; ?>}
<?php endif; ?>

<?php if ( !empty($config['appearance']['content_link']) ) : ?>
.block .block-content a, .block .block-content li a, .block .block-content li.item a,
.cms-index-index .block .block-content a, .cms-index-index .block .block-content li a, .cms-index-index .block .block-content li.item a,
.block-layered-nav .block-content dd li a,
.product-view .product-shop .no-rating a, .product-view .product-shop .ratings a,
.product-view .box-up-sell .product-name,
.data-table td a{color:<?php echo $config['appearance']['content_link']; ?>}
<?php endif; ?>

<?php if ( !empty($config['appearance']['content_link_hover']) ) : ?>
.block .block-content a:hover, .block .block-content li a:hover, .block .block-content li.item a:hover,
.cms-index-index .block .block-content a:hover, .cms-index-index .block .block-content li a:hover, .cms-index-index .block .block-content li.item a:hover,
.block-layered-nav .block-content dd li a:hover,
.product-view .product-shop .no-rating a:hover, .product-view .product-shop .ratings a:hover,
.product-view .box-up-sell .product-name:hover,
.data-table td a:hover {color:<?php echo $config['appearance']['content_link_hover']; ?>}
<?php endif; ?>

<?php if ( !empty($config['appearance']['hover_icon']) ) : ?>
.slider-banner .banner-block span img:hover, .cart-top-container .summary:hover,
.footer-subscribe button.button:hover, 
.jcarousel-prev-horizontal.jcarousel-prev-disabled-horizontal:hover, .jcarousel-next-horizontal.jcarousel-next-disabled-horizontal:hover,
.header-dropdown ul li a:hover, .currency-switch .ulSelect2 li:hover,
.data-table .btn-remove2:hover, .data-table .btn-edit2:hover,
#nav .content-block:hover img, #hellothemesNav .content-block:hover img, .jcarousel-prev:hover, .jcarousel-next:hover,
#nav>li:hover, #nav>li.over, .links-block #custom li:hover, .block-poll input[type="radio"]:checked:not(old) + label > span,
#hellothemesNav li.level-top.over, .sticker-top-right:before, #toTop:hover, #narrow-by-list dt:hover em{background-color:<?php echo $config['appearance']['hover_icon']; ?>}
<?php endif; ?>

.sticker-top-right:after{border-color:<?php echo $config['appearance']['hover_icon']; ?> rgba(0, 0, 0, 0) rgba(0, 0, 0, 0)}
.specialto{background:<?php echo $config['appearance']['hover_icon']; ?>}
.sticker-top-left:before{background-color:<?php echo $config['appearance']['color']; ?>}
.sticker-top-left:after{border-color:<?php echo $config['appearance']['color']; ?> rgba(0, 0, 0, 0) rgba(0, 0, 0, 0)}

#nav ul li a.over span, .ul-category li ul li:hover a span, #hellothemesNav li .sub-wrapper ul li ol li a:hover span,
#left-nav li ul li:hover a, #left-nav li.active a, #left-nav li.openn a, #left-nav li a:hover{color:<?php echo $config['appearance']['hover_icon']; ?> !important}

.slider-sidebar .flex-control-nav li a{background:<?php echo $config['appearance']['color']; ?>}
.slider-sidebar .flex-control-paging li a.flex-active{border-color:<?php echo $config['appearance']['hover_icon']; ?>}
.slider-sidebar .flex-control-nav li a{border-color:<?php echo $config['appearance']['color']; ?>}

<?php if ( !empty($config['header']['breadcrumb']) ) : ?>
.page-title-bg {background-color:<?php echo $config['header']['breadcrumb']; ?>}
<?php endif; ?>

<?php if ( !empty($config['appearance']['slider_border']) ) : ?>
.slider-container {border-top-color:<?php echo $config['appearance']['slider_border']; ?>}
<?php endif; ?>

<?php if ( !empty($config['header']['toolbar_bg']) ) : ?>
.top-switch-bg, .currency-switch .ulSelect2, .header-dropdown {background-color:<?php echo $config['header']['toolbar_bg']; ?>}
<?php endif; ?>

<?php if ( !empty($config['header']['header_bg']) ) : ?>
.header-wrapper, .slider-container{background-color:<?php echo $config['header']['header_bg']; ?>}
<?php endif; ?>

<?php if ( !empty($config['header']['toolbar_color']) ) : ?>
.header-switch span.current {color:<?php echo $config['header']['toolbar_color']; ?>}
.header-container .links li a, .header-switch span {color: <?php echo $config['header']['toolbar_color']; ?>}
<?php endif; ?>

<?php if ( !empty($config['header']['toolbar_hover_color']) ) : ?>
.header-container .links li a:hover {color:<?php echo $config['header']['toolbar_hover_color'] ?>}
<?php endif; ?>

<?php if ( !empty($config['footer']['footer_bg']) ) : ?>
.footer-container {background-color:<?php echo $config['footer']['footer_bg']; ?>}
<?php endif; ?>

<?php if ( !empty($config['footer']['footer_hover_color']) ) : ?>
.footer-info a:hover, .footer-info ul li a:hover {color:<?php echo $config['footer']['footer_hover_color'] ?>}
<?php endif; ?>

<?php if ( !empty($config['footer']['footer_banners_bg']) ) : ?>
.footer-banners {background-color:<?php echo $config['footer']['footer_banners_bg']; ?>}
<?php endif; ?>

<?php if ( !empty($config['footer']['footer_info_title_color']) ) : ?>
.footer-info h4 {color:<?php echo $config['footer']['footer_info_title_color']; ?>}
<?php endif; ?>

<?php if ( !empty($config['footer']['footer_info_color']) ) : ?>
.footer-info .span3 p {color:<?php echo $config['footer']['footer_info_color']; ?>}
<?php endif; ?>

.catalog-image .quick-view:hover {background:rgba(<?php echo $color_helper->hex2RGB($config['appearance']['color'], 1); ?>, 0.35)}

<?php if ( !empty($config['footer']['footer_info_link_color']) ) : ?>
.footer-info a, .footer-info ul li a {color:<?php echo $config['footer']['footer_info_link_color']; ?>}
<?php endif; ?>

<?php if ( $config['fonts']['enable_font'] && !empty($config['products']['price_font']) ) : ?>
.price-box .price {font-family:"<?php echo $config['products']['price_font']; ?>"}
<?php endif; ?>

<?php if ( $config['fonts']['enable_font'] && !empty($config['fonts']['font']) ) : ?>
body {font-family:"<?php echo $config['fonts']['font']; ?>"}
<?php endif; ?>

<?php if ( !empty($config['products']['price_color']) ) : ?>
.price, .breadcrumbs strong {color:<?php echo $config['products']['price_color']; ?>}
<?php endif; ?>

<?php if ( !empty($config['products']['price_circle_color']) ) : ?>
.price-box {background-color:<?php echo $config['products']['price_circle_color']; ?>}
<?php endif; ?>

<?php if ( !empty($config['footer']['footer_info_title_color']) ) : ?>
.footer-info h3 {color:<?php echo $config['footer']['footer_info_title_color']; ?>}
<?php endif; ?>

.navmobile #left-nav {background-color:<?php echo $config['menumobile']['mobile_bg']; ?>}

#left-nav ul.subcategory {background-color:<?php echo $config['menumobile']['mobile_submenu_bg']; ?>}

#left-nav li.active a, #left-nav li.openn a, #left-nav li a {color:<?php echo $config['menumobile']['mobile_link']; ?>}

#left-nav li ul li a {color:<?php echo $config['menumobile']['mobile_submenu_link']; ?>}