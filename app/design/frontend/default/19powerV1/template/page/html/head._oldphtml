<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    design
 * @package     base_default
 * @copyright   Copyright (c) 2011 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */
?>
<?php 
$config = Mage::getStoreConfig('hellothemessettings', $_GET['store']);
$config_slideshow   = Mage::getStoreConfig('hellothemesslideshow', Mage::app()->getStore()->getId());
?>
<meta http-equiv="Content-Type" content="<?php echo $this->getContentType() ?>" />
<?php if (Mage::getStoreConfig('hellothemessettings/design/responsive')) : ?>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no" />
<?php else : ?>
<meta name="viewport" content="width=device-width" />
<?php endif; ?>
<meta http-equiv="Content-Type" content="<?php echo $this->getContentType() ?>" />
<title><?php echo $this->getTitle() ?></title>
<meta name="description" content="<?php echo htmlspecialchars($this->getDescription()) ?>" />
<meta name="keywords" content="<?php echo htmlspecialchars($this->getKeywords()) ?>" />
<meta name="robots" content="<?php echo htmlspecialchars($this->getRobots()) ?>" />

<!-- AddThis -->
<?php if (Mage::registry('current_product')) : ?>
<meta property="og:image" content="<?php echo Mage::helper('catalog/image')->init(Mage::registry('current_product'), 'small_image')->resize(100,100);?>" />
<meta property="og:title" content="<?php echo Mage::registry('current_product')->getMetaTitle()?>" />
<?php endif;?>
<!-- End AddThis -->

<link rel="icon" href="<?php echo $this->getFaviconFile(); ?>" type="image/x-icon" />
<link rel="shortcut icon" href="<?php echo $this->getFaviconFile(); ?>" type="image/x-icon" />

<link rel="apple-touch-icon" href="<?php echo Mage::getBaseUrl('media') . "/hellothemes/mobile/" . Mage::getStoreConfig('hellothemessettings/mobile/icon57') ?>" />
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo Mage::getBaseUrl('media') . "/hellothemes/mobile/" . Mage::getStoreConfig('hellothemessettings/mobile/icon72') ?>" />
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo Mage::getBaseUrl('media') . "/hellothemes/mobile/" . Mage::getStoreConfig('hellothemessettings/mobile/icon114') ?>" />

<!-- Core JS --> 
<?php echo $this->getCssJsHtml() ?>
<!-- /Core JS -->

<!-- HelloThemes Framework --> 
<?php if (Mage::helper('hellothemesslideshow/data')->isSlideshowEnabled() && $config_slideshow['config']['slider'] == 'revolution') : ?>
<script type="text/javascript" src="<?php echo Mage::getBaseUrl('js') . 'hellothemes/jquery.themepunch.plugins.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo Mage::getBaseUrl('js') . 'hellothemes/jquery.themepunch.revolution.min.js'; ?>"></script>
<?php endif; ?>

<?php if ($config['fonts']['enable_font']) :?>
	<link href='//fonts.googleapis.com/css?family=<?php echo str_replace(' ', '+', $config['fonts']['font']); ?>:300,400,700' rel='stylesheet' type='text/css' />
	<?php if ($config['fonts']['price_font'] != $config['fonts']['font'] ) : ?>
		<link href='//fonts.googleapis.com/css?family=<?php echo str_replace(' ', '+', $config['products']['price_font']); ?>:300,400,700' rel='stylesheet' type='text/css' />
    <?php endif; ?>
<?php endif; ?>

<link href='<?php echo $this->getSkinUrl('css/framework.css.php'); echo '?store='.Mage::app()->getStore()->getCode(); ?>' rel='stylesheet' type='text/css' />
<!-- /HelloThemes Framework -->

<?php echo $this->getChildHtml() ?>

<?php echo $this->helper('core/js')->getTranslatorScript() ?>

<script type="text/javascript">
    //<![CDATA[
    var Hellothemes = {};
    Hellothemes.price_circle = <?php echo Mage::getStoreConfig('hellothemessettings/ajax/price_circle') ?>;
    Hellothemes.totop = <?php echo Mage::getStoreConfig('hellothemessettings/ajax/totop') ?>;
    Hellothemes.responsive = <?php echo Mage::getStoreConfig('hellothemessettings/design/responsive') ?>;
    Hellothemes.text = {};
    Hellothemes.text.more = '<?php echo $this->__('more...') ?>';
    Hellothemes.text.less = '<?php echo $this->__('less...') ?>';
    Hellothemes.anystretch_bg = '';
    <?php
    $bg_image = '';
    if (!empty($config['content_bg_img']) && $config['content_bg_img_mode'] == 'stretch') {
        $bg_image = $config['content_bg_img'];
    }

    $route = Mage::app()->getFrontController()->getRequest()->getRouteName();
    $action = Mage::app()->getFrontController()->getRequest()->getActionName();
    if ( ($route == 'customer' && ($action == 'login' || $action == 'forgotpassword' || $action == 'create')) && !empty($config['login_bg']) ) {
        $bg_image = $config['login_bg'];
    }

    ?>
    //]]>
</script>

<?php echo $this->getIncludes() ?>