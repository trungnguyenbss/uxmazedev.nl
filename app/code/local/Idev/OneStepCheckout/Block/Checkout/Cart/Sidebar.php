<?php
class Idev_OneStepCheckout_Block_Checkout_Cart_Sidebar extends Idev_OneStepCheckout_Block_Checkout_Cart_Sidebar_Amasty_Pure
{
    /**
     * Get one page checkout page url
     *
     * @return bool
     */
    public function getCheckoutUrl()
    {
        if (!$this->helper('onestepcheckout')->isRewriteCheckoutLinksEnabled()){
            return parent::getCheckoutUrl();
        }
        return $this->getUrl('onestepcheckout', array('_secure'=>true));
    }
}
