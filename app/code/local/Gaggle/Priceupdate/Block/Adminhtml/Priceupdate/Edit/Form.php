<?php
/**
 *
 * @category    Gaggle
 * @package     Gaggle_Priceupdate
 * @author      gaggle.thread@gmail.com
 */
class Gaggle_Priceupdate_Block_Adminhtml_Priceupdate_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Prepare form before rendering HTML.
     *
     * 
     */
    protected function _prepareForm()
    {
        $helper = Mage::helper('priceupdate');
        $form = new Varien_Data_Form(array(
            'id'     => 'edit_form',
            'action' => $this->getUrl('*/*/apply'),
            'method' => 'post'
        ));
        $fieldset = $form->addFieldset('base_fieldset', array('legend' => $helper->__('Price Update')));
        $fieldset->addField('category', 'select', array(
            'name'     => 'category',
            'title'    => Mage::helper('priceupdate')->__('Category'),
            'label'    => Mage::helper('priceupdate')->__('Category'),
            'required' => false,
            'values'   => $this->getCategories()
        ));
		 $fieldset->addField('price_type', 'select', array(
            'name'     => 'price_type',
            'title'    => Mage::helper('priceupdate')->__('Price Type'),
            'label'    => Mage::helper('priceupdate')->__('Price Type'),
            'required' => false,
            'values'   => array('fixed'=>'Fixed','percentage'=>'Percentage')
        ));
		$fieldset->addField('attribute', 'multiselect', array(
            'name'     => 'attribute',
            'title'    => Mage::helper('priceupdate')->__('Attribute'),
            'label'    => Mage::helper('priceupdate')->__('Attribute'),
            'required' => true,
            'values'   => $this->getPriceAttributes()
        ));
        $fieldset->addField('price','text', array(
            'name'     => 'price',
            'title'    => $helper->__('Price'),
            'label'    => $helper->__('Price'),
            'required' => true,
           
        ));
		$fieldset->addField('label','label', array(
            'name'     => 'label',
            'title'    => $helper->__('Price'),
            'label'    => $helper->__(''),
			'value'	   =>'Need Custom extension ? Contact us at gaggle.thread@gmail.com.',
			'bold'      =>  true,
           
           
        ));

        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
	function getWebsites()
	{
		$websites=array();
		//$websites[0]='All Websites';
		foreach (Mage::app()->getWebsites() as $website) {
			foreach ($website->getGroups() as $group) {
				$stores = $group->getStores();
				foreach ($stores as $store) {
					
					$websites[$website->getId()]=$website->getName();
					break;
					//$store is a store object
				}
			}
		}
		return $websites;
	}
	function getCategories()
	{
		$result=array();
		$result[0]='All';
		$categories = Mage::getModel('catalog/category')->getCollection()
			->addAttributeToSelect('id')
			->addAttributeToSelect('name')
			->addAttributeToSelect('is_active');
		foreach($categories as $category)
		{
			$result[$category->getId()]=$category->getName().' ('.$category->getId().')';
		}
		return $result;
	}
	
	function getPriceAttributes()
	{
		$result=array();
		$result[]=array('label'=>'Custom Options Price','value'=>'custom');
		$result[]=array('label'=>'Config Options Price','value'=>'config_options');
		$result[]=array('label'=>'Price Attribute','value'=>'price');
		$result[]=array('label'=>'Special Price','value'=>'special_price');
		$result[]=array('label'=>'Msrp','value'=>'msrp');;
		$result[]=array('label'=>'Group Price','value'=>'group_price');;
		$result[]=array('label'=>'Tier Price','value'=>'tier_price');;
		
		return $result;
	}
	
}
