<?php
class Gaggle_Priceupdate_Block_Adminhtml_Priceupdate_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct()
    {
        parent::__construct();
        $this->removeButton('back')
            ->removeButton('reset');
         /*     ->removeButton('save'); */
		$this->_updateButton('save', 'label', Mage::helper('priceupdate')->__('Apply Price'));
		
		if($this->canRollback())
		{
			 $this->addButton('roll_back', array(
				'label'     => Mage::helper('priceupdate')->__('Roll Back Last Update'),
				'onclick'   => "confirmSetLocation('Are you sure ?', '{$this->getUrl('priceupdate/adminhtml_index/rollback')}')",
			));
		}
    }
	protected function _construct()
    {
        parent::_construct();

        $this->_objectId   = 'priceupdate_id';
        $this->_blockGroup = 'priceupdate';
        $this->_controller = 'adminhtml_priceupdate';
	}

    /**
     * Get header text
     *
     * @return string
     */
    public function getHeaderText()
    {
        return Mage::helper('importexport')->__('Don\'t forget to take backup of db before performing any operation.');
    }
	public function canRollback()
	{
		$dir=Mage::getBaseDir('media').DS.'gaggle'.DS.'priceupdate';
		
		if(count(scandir($dir))>2)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}