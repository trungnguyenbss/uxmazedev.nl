<?php 
class Gaggle_Priceupdate_Adminhtml_IndexController extends Mage_Adminhtml_Controller_Action
{

	public $backupTables=array();
	public function indexAction()
	{
		
	 $this->loadLayout();
	 $this->_addContent($this->getLayout()->createBlock('priceupdate/adminhtml_priceupdate_edit'));
	$this->renderLayout();	

	}
	
	/*
	 * Action For Apply price updates
	 */
	public function applyAction()
	{
		ini_set('memory_limit','256M');
		if($data=$this->getRequest()->getPost())
		{
			if($data['category']==0)
			{
				$products=Mage::getModel('catalog/product')->getCollection()/* ->addAttributeToSelect('*') */;
			}
			else
			{
				$products = Mage::getSingleton('catalog/category')->load($data['category'])
					->getProductCollection();
					
			}
			foreach($data['attribute'] as $attribute)
			{
				switch($attribute)
				{
					case 'custom':		$this->updateCustomPrice($products,$data);
										break;
					case 'config_options':		$this->updateConfigOptionPrice($products,$data);
										break;
					case 'price':		$this->updatePrice($products,$data);
										break;
					case 'special_price':
										$this->updateSpecialPrice($products,$data);
										break;
					case 'msrp':		$this->updateMsrpPrice($products,$data);
										break;
					case 'group_price':
										$this->updateGroupPrice($products,$data);
										break;
					case 'tier_price':
										$this->updateTierPrice($products,$data);
										break;
						
				}
			}
			
			/*Code for running price indexing*/
			$process = Mage::getModel('index/process')->load(2);
			$process->reindexAll();
		}
		Mage::getSingleton('core/session')->addSuccess('Selected Price\'s are applied successfully..');
		$this->_redirect('*/*/index');
		

	}
	
	/*
	 *	Funcion for updating price of Config product options
	 */
	function updateConfigOptionPrice($products,$data)
	{
		
		$resource = Mage::getSingleton('core/resource');
		$writeConnection = $resource->getConnection('core_write');
		if($data['category']==0)
		{
			$table = $resource->getTableName('catalog_product_super_attribute_pricing');
				if($data['price_type']=='fixed')
				{
					$price=$data['price'];
					$query="UPDATE`{$table}` SET `pricing_value` = pricing_value+{$price} ;" ;
				}
				else
				{
					$price=(100+$data['price'])/100;
					$query="UPDATE`{$table}` SET `pricing_value` = pricing_value*{$price} ;" ;
				}
				$writeConnection->query($query);
		}
		else{
			foreach($products as $product)
			{
			 $table = $resource->getTableName('catalog_product_super_attribute');
			 $query="SELECT product_super_attribute_id,product_id FROM {$table} WHERE product_id='{$product->getId()}'";
			 $results = $writeConnection->fetchAll($query);
			 foreach($results as $row)
			 {
				$table = $resource->getTableName('catalog_product_super_attribute_pricing');
				if($data['price_type']=='fixed')
				{
					$price=$data['price'];
					$query="UPDATE`{$table}` SET `pricing_value` = pricing_value+{$price} WHERE `product_super_attribute_id` = '{$row['product_super_attribute_id']}';" ;
				}
				else
				{
					$price=(100+$data['price'])/100;
					$query="UPDATE`{$table}` SET `pricing_value` = pricing_value*{$price} WHERE `product_super_attribute_id` = '{$row['product_super_attribute_id']}';" ;
				}
				$writeConnection->query($query);
			 }
			}
		}
	}
	
	/*
	 *	Funcion for updating price of Custom options
	 */
	function updateCustomPrice($products,$data)
	{	
		$resource = Mage::getSingleton('core/resource');
		$writeConnection = $resource->getConnection('core_write');
		
		if($data['category']==0)
		{
		
			
			$table = $resource->getTableName('catalog_product_option_price');
			
				if($data['price_type']=='fixed')
				{
					$price=$data['price'];
					$this->exportCsv($table);
					$query="UPDATE`{$table}` SET `price` = price+{$price} ;" ;
					$writeConnection->query($query);
					$this->exportCsv($table);
					$table = $resource->getTableName('catalog_product_option_type_price');
					$query="UPDATE`{$table}` SET `price` = price+{$price};" ;
					$writeConnection->query($query);
				}
				else
				{
					$price=(100+$data['price'])/100;
					$this->exportCsv($table);
					$query="UPDATE`{$table}` SET `price` = price*{$price} ;" ;
					$writeConnection->query($query);
					$table = $resource->getTableName('catalog_product_option_type_price');
					$this->exportCsv($table);
					$query="UPDATE`{$table}` SET `price` = price*{$price};" ;
					$writeConnection->query($query);
				}
				
				
		}
		else{
			foreach($products as $product)
			{
			 $table = $resource->getTableName('catalog_product_option');
			 $query="SELECT option_id,product_id,type FROM {$table} WHERE product_id='{$product->getId()}'";
			 $results = $writeConnection->fetchAll($query);
			 foreach($results as $row)
			 {
				
				if($row['type']=='drop_down'||$row['type']=='checkbox'||$row['type']=='radio')
				{
					
					$table = $resource->getTableName('catalog_product_option_type_value');
					$this->exportCsv($table);
					$query="SELECT option_type_id,option_id FROM {$table} WHERE option_id='{$row['option_id']}'";
					$results1 = $writeConnection->fetchAll($query);
					foreach($results1 as $row1)
					{
						
						$table = $resource->getTableName('catalog_product_option_type_price');
						if($data['price_type']=='fixed')
						{
							$price=$data['price'];
							$query="UPDATE`{$table}` SET `price` = price+{$price} WHERE `option_type_id` = '{$row1['option_type_id']}';" ;
						}
						else
						{
							$price=(100+$data['price'])/100;
							$query="UPDATE`{$table}` SET `price` = price*{$price} WHERE `option_type_id` = '{$row1['option_type_id']}';" ;
						}
						$writeConnection->query($query);
						
					}
				}
				else
				{
					$table = $resource->getTableName('catalog_product_option_price');
					$this->exportCsv($table);
					if($data['price_type']=='fixed')
					{
						$price=$data['price'];
						$query="UPDATE`{$table}` SET `price` = price+{$price} WHERE `option_id` = '{$row['option_id']}';" ;
					}
					else
					{
						$price=(100+$data['price'])/100;
						$query="UPDATE`{$table}` SET `price` = price*{$price} WHERE `option_id` = '{$row['option_id']}';" ;
					}
					$writeConnection->query($query);
				}
			 }
			}
		}
	
	}
	
	/*
	 *	Funcion for updating price
	 */
	function updatePrice($products,$data)
	{
		$attr_id=$this->getAttrIdByCode('price');
		$resource = Mage::getSingleton('core/resource');
		$writeConnection = $resource->getConnection('core_write');	
		$table = $resource->getTableName('catalog_product_entity_decimal');
		$this->exportCsv($table);
		if($data['category']==0)
		{
			if($data['price_type']=='fixed')
			{
				$price=$data['price'];
				$query="UPDATE`{$table}` SET `value` = value+{$price} WHERE `attribute_id` = '{$attr_id}';" ;
			}
			else
			{
				$price=(100+$data['price'])/100;
				$query="UPDATE`{$table}` SET `value` = value*{$price} WHERE `attribute_id` = '{$attr_id}';" ;
				
			}
			$writeConnection->query($query);
		}
		else
		{
			foreach($products as $product)
			{
			
				
				if($data['price_type']=='fixed')
				{
					$price=$data['price'];
					$query="UPDATE`{$table}` SET `value` = value+{$price} WHERE `attribute_id` = '{$attr_id}' AND `entity_id`='{$product->getId()}';" ;
				}
				else
				{
					$price=(100+$data['price'])/100;
					$query="UPDATE`{$table}` SET `value` = value*{$price} WHERE `attribute_id` = '{$attr_id}' AND `entity_id`='{$product->getId()}';" ;
				}
				$writeConnection->query($query);
			 
			}
		}
	
	}
	
	/*
	 *	Funcion for updating special price
	 */
	function updateSpecialPrice($products,$data)
	{
		$attr_id=$this->getAttrIdByCode('special_price');
		$resource = Mage::getSingleton('core/resource');
		$writeConnection = $resource->getConnection('core_write');	
		$table = $resource->getTableName('catalog_product_entity_decimal');
		$this->exportCsv($table);
		if($data['category']==0)
		{
			if($data['price_type']=='fixed')
			{
				$price=$data['price'];
				$query="UPDATE`{$table}` SET `value` = value+{$price} WHERE `attribute_id` = '{$attr_id}';" ;
			}
			else
			{
				$price=(100+$data['price'])/100;
				$query="UPDATE`{$table}` SET `value` = value*{$price} WHERE `attribute_id` = '{$attr_id}';" ;
				
			}
			$writeConnection->query($query);
		}
		else
		{
			foreach($products as $product)
			{
			
				
				if($data['price_type']=='fixed')
				{
					$price=$data['price'];
					$query="UPDATE`{$table}` SET `value` = value+{$price} WHERE `attribute_id` = '{$attr_id}' AND `entity_id`='{$product->getId()}';" ;
				}
				else
				{
					$price=(100+$data['price'])/100;
					$query="UPDATE`{$table}` SET `value` = value*{$price} WHERE `attribute_id` = '{$attr_id}' AND `entity_id`='{$product->getId()}';" ;
				}
				$writeConnection->query($query);
			 
			}
		}
	
	}
	
	/*
	 *	Funcion for updating msrp price 
	 */
	function updateMsrpPrice($products,$data)
	{	
		$attr_id=$this->getAttrIdByCode('msrp');
		$resource = Mage::getSingleton('core/resource');
		$writeConnection = $resource->getConnection('core_write');	
		$table = $resource->getTableName('catalog_product_entity_decimal');
		$this->exportCsv($table);
		if($data['category']==0)
		{
			if($data['price_type']=='fixed')
			{
				$price=$data['price'];
				$query="UPDATE`{$table}` SET `value` = value+{$price} WHERE `attribute_id` = '{$attr_id}';" ;
			}
			else
			{
				$price=(100+$data['price'])/100;
				$query="UPDATE`{$table}` SET `value` = value*{$price} WHERE `attribute_id` = '{$attr_id}';" ;
				
			}
			$writeConnection->query($query);
		}
		else
		{
			foreach($products as $product)
			{
			
				
				if($data['price_type']=='fixed')
				{
					$price=$data['price'];
					$query="UPDATE`{$table}` SET `value` = value+{$price} WHERE `attribute_id` = '{$attr_id}' AND `entity_id`='{$product->getId()}';" ;
				}
				else
				{
					$price=(100+$data['price'])/100;
					$query="UPDATE`{$table}` SET `value` = value*{$price} WHERE `attribute_id` = '{$attr_id}' AND `entity_id`='{$product->getId()}';" ;
				}
				$writeConnection->query($query);
			 
			}
		}
	
	}
	
	/*
	 *	Funcion for updating Tier price
	 */
	function updateTierPrice($products,$data)
	{
		
		$resource = Mage::getSingleton('core/resource');
		$writeConnection = $resource->getConnection('core_write');	
		$table = $resource->getTableName('catalog_product_entity_tier_price');
		$this->exportCsv($table);
		if($data['category']==0)
		{
			if($data['price_type']=='fixed')
				{
					$price=$data['price'];
					$query="UPDATE`{$table}` SET `value` = value+{$price};" ;
				}
				else
				{
					$price=(100+$data['price'])/100;
					$query="UPDATE`{$table}` SET `value` = value*{$price};" ;
				}
			
			$writeConnection->query($query);
		}
		else
		{
			foreach($products as $product)
			{
			
				
				if($data['price_type']=='fixed')
				{
					$price=$data['price'];
					$query="UPDATE`{$table}` SET `value` = value+{$price} WHERE `entity_id`='{$product->getId()}';" ;
				}
				else
				{
					$price=(100+$data['price'])/100;
					$query="UPDATE`{$table}` SET `value` = value*{$price} WHERE `entity_id`='{$product->getId()}';" ;
				}
				$writeConnection->query($query);
			 
			}
		}
	
	}
	
	/*
	 *	Funcion for updating Group price
	 */
	function updateGroupPrice($products,$data)
	{	
		
		$resource = Mage::getSingleton('core/resource');
		$writeConnection = $resource->getConnection('core_write');	
		$table = $resource->getTableName('catalog_product_entity_group_price');
		$this->exportCsv($table);
		if($data['category']==0)
		{
			if($data['price_type']=='fixed')
				{
					$price=$data['price'];
					$query="UPDATE`{$table}` SET `value` = value+{$price};" ;
				}
				else
				{
					$price=(100+$data['price'])/100;
					$query="UPDATE`{$table}` SET `value` = value*{$price};" ;
				}
			
			$writeConnection->query($query);
		}
		else
		{
			foreach($products as $product)
			{
			
				
				if($data['price_type']=='fixed')
				{
					$price=$data['price'];
					$query="UPDATE`{$table}` SET `value` = value+{$price} WHERE `entity_id`='{$product->getId()}';" ;
				}
				else
				{
					$price=(100+$data['price'])/100;
					$query="UPDATE`{$table}` SET `value` = value*{$price} WHERE `entity_id`='{$product->getId()}';" ;
				}
				$writeConnection->query($query);
			 
			}
		}
	
	}
	public function exportCsv($table)
	{	
		if (!file_exists(Mage::getBaseDir('media').DS.'gaggle'.DS.'priceupdate')) {
			mkdir(Mage::getBaseDir('media').DS.'gaggle'.DS.'priceupdate', 0777, true);
		}
		if(in_array($table,$this->backupTables))
		{
			return;
		}
		else
		{
			$this->backupTables[]=$table;
		}
		$resource = Mage::getSingleton('core/resource');
		$writeConnection = $resource->getConnection('core_write');	
		$table = $resource->getTableName($table);
		$query="SELECT * FROM {$table}";
		$results=$results1 = $writeConnection->fetchAll($query);
		$header=array();
		foreach($results1 as $rows)
		{
			foreach($rows as $key=>$val)
			{
				$header[]=$key;
			}
			break;
		}
		$sql = fopen(Mage::getBaseDir('media').DS.'gaggle'.DS.'priceupdate'.DS."{$table}.sql", 'w');
		$query="TRUNCATE `{$table}`;";
		fwrite($sql,$query.PHP_EOL);
		foreach($results1 as $rows)
		{	
			$query="INSERT INTO `{$table}`  ";
			$row=array();
			$i=0;
			$keys='';
			$values='';
			foreach($rows as $key=>$val)
			{
				if($i++==0)
				{
					$keys.="`{$key}`";
					$values.="'{$val}'";
				}
				else
				{	
					$keys.=",`{$key}`";
					$values.=",'{$val}'";
				}
			}
			$query.="({$keys}) VALUES ({$values});";
			fwrite($sql,$query.PHP_EOL);
		}
		fclose($sql); 
		return;
		
		
		$finsh=array();
		$finsh[]=$header;
		foreach($results1 as $rows)
		{
			$row=array();
			foreach($rows as $key=>$val)
			{
				$row[]=$val;
			}
			$finsh[]=$row;
		}
		
		$file = fopen(Mage::getBaseDir('media').DS.'gaggle'.DS.'priceupdate'.DS."{$table}.csv","w");

		foreach ($finsh as $line)
		  {
		  fputcsv($file,$line);
		  }

		fclose($file); 
		echo 'Done';
	}
	public function rollbackAction()
	{
		
		$dir=Mage::getBaseDir('media').DS.'gaggle'.DS.'priceupdate';
		
		foreach(scandir($dir) as $file)
		{
			if($file=='.'||$file=='..')
			{
				continue;
			}
			else
			{
				
				$table=str_replace('.csv','',$file);
				
				$file=$dir.DS.$file;
				$this->recoverData($table,$file);
				unlink($file);
			}
		}
		
		Mage::getSingleton('core/session')->addSuccess('Price rolled back successfully..');
		$this->_redirect('*/*/index');
	}
	
	/*Function For roll back last update*/
	public function recoverData($table,$file)
	{
		
		$query=file_get_contents($file);
		$resource = Mage::getSingleton('core/resource');
		$writeConnection = $resource->getConnection('core_write');	
		$table = $resource->getTableName($table);
		$writeConnection->query($query);
	
	}
	
	/**Function for getting attribute id by code **/
	public function getAttrIdByCode($code)
	{
		$eavAttribute = new Mage_Eav_Model_Mysql4_Entity_Attribute();
		$attr_id = $eavAttribute->getIdByCode('catalog_product', $code);
		return $attr_id;
	}
}
?>