<?php
/**
* dasENIGMA.
*
* NOTICE OF LICENSE
*
* This source file is subject to the EULA
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://codecanyon.net/licenses/regular
*
* =================================================================
*                 MAGENTO EDITION USAGE NOTICE
* =================================================================
* This package designed for Magento community edition
* dasENIGMA does not guarantee correct work of this extension
* on any other Magento edition except Magento community edition.
* dasENIGMA does not provide extension support in case of
* incorrect edition usage.
* =================================================================
*
* @category   Enigma
* @package    Enigma_LandingPages
* @version    1.0
* @copyright  Copyright (c) 2014 dasENIGMA. (http://codecanyon.net/user/dasEnigma/portfolio?ref=dasEnigma)
* @license    http://codecanyon.net/licenses/regular
*/
class Enigma_Xlanding_PageController extends Mage_Core_Controller_Front_Action{
    public function viewAction(){
    	$hlp = Mage::helper('elanding');    	
        $pageId = $this->getRequest()->getParam('page_id', null);  
        $page = Mage::getModel('elanding/page')->load($pageId);
        if (!$page) {
        	return;
        }
		
        Mage::register('elanding_page', $page);        
        $page->applyPageRules();
        $opt = $page->getAttributesAsArray();
        Mage::register('elanding_attributes', $opt);        
        $this->loadLayout();        
        $this->enableLayeredNavigation();
		$root = $this->getLayout()->getBlock('root');
		if ($root) {
			$pageLayout = $page->getRootTemplate();
			if ($pageLayout != 'empty') {
	            $this->getLayout()->helper('page/layout')->applyTemplate($pageLayout);
			}
			
			$layoutUpdate = $page->getLayoutUpdateXml();
			if ($layoutUpdate != '') {
				$this->loadLayoutUpdates();
        		$this->getLayout()->getUpdate()->addUpdate($layoutUpdate);
        		$this->generateLayoutXml()->generateLayoutBlocks();
			}
		}
		
		$head = $this->getLayout()->getBlock('head');		
		$url = Mage::getBaseUrl() . $page->getIdentifier() . Mage::getStoreConfig('catalog/seo/category_url_suffix'); 
		$head->addLinkRel('canonical', $url);
		 
		if ($page->getMetaTitle() != '') {
			$head->setTitle($this->trim($page->getMetaTitle()));
		}
		if ($page->getMetaKeywords() != '') { 
			$head->setKeywords($this->trim($page->getMetaKeywords()));
		}
		if ($page->getMetaDescription() != '') {
            $head->setDescription($this->trim($page->getMetaDescription()));
		}
		
		$list = $this->getLayout()->getBlock('product_list');
		if ($list) {
			$list->setColumnCount($page->getColumnsCount() > 0 ? $page->getColumnsCount() : $hlp->getColumnCount());
		}

		if ($topBlock = $page->getLayoutStaticTop()) {
			$this->getLayout()->getBlock('content')->insert($this->getLayout()->createBlock('cms/block')->setBlockId($topBlock));	
		}
		
    	if ($bottomBlock = $page->getLayoutStaticBottom()) {
			$this->getLayout()->getBlock('content')->append($this->getLayout()->createBlock('cms/block')->setBlockId($bottomBlock));	
		}
		
		$this->includeNavigation($page);
		
        if ('true' === (string)Mage::getConfig()->getNode('modules/Enigma_Shopby/active')){
            Mage::getSingleton('eshopby/observer')->handleLayoutRender();
        }	
        	
		$this->renderLayout();
    }
	
    private function includeNavigation($page){
    	if ($page->getIncludeNavigation() === Enigma_Xlanding_Model_Source_Navigation::INCLUDE_NO) {
    		return;
    	}
    	
    	$block = $this->getLayout()->getBlock('eshopby.navleft');
    	$classExists = false;
    	if ('true' == (string)Mage::getConfig()->getNode('modules/Enigma_Shopby/active')){
    	   $classExists = class_exists('Enigma_Shopby_Block_Catalog_Layer_View');
    	}
    	
    	if (!$block && $classExists) {
			$block = $this->getLayout()->createBlock(
				'Enigma_Shopby_Block_Catalog_Layer_View',
				'eshopby.navleft',
				array(
					'template' => 'catalog/layer/view.phtml',
				)
			);
			
			$blockPlacement = $page->getIncludeNavigation();
			
			$container = $this->getLayout()->getBlock($blockPlacement);
			if ($container) {
				$container->insert($block);
			}
			
			if (0) {
    			$blocks = array(
                    array(
                        'name' => 'Enigma_Shopby_Block_Catalog_Layer_View_Top',
                        'alias' => 'eshopby.navtop',
                        'template' => 'eshopby/view_top.phtml',
                        'placement' => 'content'
                    ),
                    array(
                        'name' => 'Enigma_Shopby_Block_Top',
                        'alias' => 'eshopby.top',
                        'template' => 'eshopby/top.phtml',
                        'placement' => 'content'
                    ),
            	);
        	
        	    foreach ($blocks as $item) {
                    if (!class_exists($item['name'])) {
                        continue;
                    }
                
                    $block = $this->getLayout()->createBlock(
        				$item['name'],
        				$item['alias'],
        				array('template' => $item['template'])
        			);
    			
    			    $container = $this->getLayout()->getBlock($item['placement']);
    			    if ($container) {
    				    $container->insert($block);
    			    }
                }
			}			
			return;
    	}
		
    	$block = $this->getLayout()->getBlock('catalog.leftnav');
    	if (!$block) {
	    	$block = $this->getLayout()->createBlock(
			'Mage_Catalog_Block_Layer_View',
				'catalog.leftnav',
				array('template' => 'catalog/layer/view.phtml',)
			);
			
			$blockPlacement = $page->getIncludeNavigation();
			
			$container = $this->getLayout()->getBlock($blockPlacement);
			if ($container) {
				$container->insert($block);
			}
    	}
    }
	
    private function enableLayeredNavigation(){
    	if (!Mage::helper('elanding')->seoLinksActive()) {
    		return false;
    	}
    	
    	$categoryId = (int) Mage::app()->getStore()->getRootCategoryId();
        if (!$categoryId) {
            $this->_forward('noRoute'); 
            return;
        }

        $category = Mage::getModel('catalog/category')->setStoreId(Mage::app()->getStore()->getId())->load($categoryId);
            
        Mage::register('current_category', $category); 
        Mage::getSingleton('catalog/session')->setLastVisitedCategoryId($category->getId());  
          
        try {
            Mage::dispatchEvent('catalog_controller_category_init_after', 
                array('category' => $category, 'controller_action' => $this));
        } catch (Mage_Core_Exception $e) {
            Mage::logException($e);
            return;
        }
    }
    
	private function trim($str){
        $str = strip_tags($str);
        $str = str_replace('"', '', $str);
        return trim($str, " -");
    } 
}