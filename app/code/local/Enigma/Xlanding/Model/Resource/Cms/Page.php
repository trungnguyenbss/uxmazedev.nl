<?php
/**
* dasENIGMA.
*
* NOTICE OF LICENSE
*
* This source file is subject to the EULA
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://codecanyon.net/licenses/regular
*
* =================================================================
*                 MAGENTO EDITION USAGE NOTICE
* =================================================================
* This package designed for Magento community edition
* dasENIGMA does not guarantee correct work of this extension
* on any other Magento edition except Magento community edition.
* dasENIGMA does not provide extension support in case of
* incorrect edition usage.
* =================================================================
*
* @category   Enigma
* @package    Enigma_LandingPages
* @version    1.0
* @copyright  Copyright (c) 2014 dasENIGMA. (http://codecanyon.net/user/dasEnigma/portfolio?ref=dasEnigma)
* @license    http://codecanyon.net/licenses/regular
*/
class Enigma_Xlanding_Model_Resource_Cms_Page extends Mage_Sitemap_Model_Resource_Cms_Page {
	public function getCollection($storeId){
		$pages = parent::getCollection($storeId);
		
		$select = Mage::getModel('elanding/page')->getCollection()
			->addFieldToFilter('is_active', 1)
			->getSelect()
			->join(
                array('elanding_page_store' => $this->getTable('elanding/page_store')),
                'main_table.page_id = elanding_page_store.page_id',
                array())
                ->where('elanding_page_store.store_id IN (?)', array($storeId));
                
        $query = $this->_getWriteAdapter()->query($select);
                
	 	while ($row = $query->fetch()) {
	 		$object = new Varien_Object();
        	$object->setId($row['page_id']);
        	$object->setUrl($row['identifier']);
			$pages[] = $object;
        }	        
		return $pages;
	}
}