<?php
/**
* dasENIGMA.
*
* NOTICE OF LICENSE
*
* This source file is subject to the EULA
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://codecanyon.net/licenses/regular
*
* =================================================================
*                 MAGENTO EDITION USAGE NOTICE
* =================================================================
* This package designed for Magento community edition
* dasENIGMA does not guarantee correct work of this extension
* on any other Magento edition except Magento community edition.
* dasENIGMA does not provide extension support in case of
* incorrect edition usage.
* =================================================================
*
* @category   Enigma
* @package    Enigma_LandingPages
* @version    1.0
* @copyright  Copyright (c) 2014 dasENIGMA. (http://codecanyon.net/user/dasEnigma/portfolio?ref=dasEnigma)
* @license    http://codecanyon.net/licenses/regular
*/
class Enigma_Xlanding_Model_Mysql4_Import extends Mage_Core_Model_Mysql4_Abstract{
	private $_columns = array();
	private $_existingPages = array();
	private $_importErrors = array();
	
  	protected function _construct(){
        $this->_init('elanding/page', 'page_id');
    }
    
	public function uploadAndImport(Varien_Object $object){
        if (empty($_FILES['groups']['tmp_name']['import']['fields']['file']['value'])) {
            return $this;
        }

        $csvFile = $_FILES['groups']['tmp_name']['import']['fields']['file']['value'];
        $website = Mage::app()->getWebsite($object->getScopeId());

        $this->_importWebsiteId = (int)$website->getId();
        $this->_importUniqueHash = array();
        $this->_importErrors = array();
        $this->_importedRows = 0;

        $io = new Varien_Io_File();
        $info = pathinfo($csvFile);
        $io->open(array('path' => $info['dirname']));
        $io->streamOpen($info['basename'], 'r');
		
        $headers = $io->streamReadCsv();
        if ($headers === false || count($headers) < 5) {
            $io->streamClose();
            Mage::throwException(Mage::helper('elanding')->__('Invalid Landing Pages File Format'));
        } else {
        	$this->_columns = $headers;
        }
        
        $adapter = $this->_getWriteAdapter();
        $adapter->beginTransaction();

        try {
            $rowNumber  = 1;
            $importData = array();            
            $this->loadExistingPages();

            while (false !== ($csvLine = $io->streamReadCsv())) {
                $rowNumber ++;

                if (empty($csvLine)) {
                    continue;
                }

                $row = $this->validateCSVRow($csvLine, $rowNumber);
                if ($row !== false) {
                    $importData[] = $row;
                }

                if (count($importData) == 5000) {
                    $this->saveImportData($importData);
                    $importData = array();
                }
            }
            $this->saveImportData($importData);
            $io->streamClose();
        } catch (Mage_Core_Exception $e) {
            $adapter->rollback();
            $io->streamClose();
            Mage::throwException($e->getMessage());
        } catch (Exception $e) {
            $adapter->rollback();
            $io->streamClose();
            Mage::logException($e);
            Mage::throwException(Mage::helper('elanding')->__('An error occurred while import Landing Pages.'));
        }

        $adapter->commit();

        if ($this->_importErrors) {
            $error = Mage::helper('shipping')->__('Landing Pages has not been imported completely. See the following list of errors:<br /> %s', implode(" \n", $this->_importErrors));
            Mage::throwException($error);
        }
        return $this;
    }
    
    public function validateCSVRow($line, $rowNumber){
    	if (count($this->_columns) != count($line)) {
    		$this->_importErrors[] = Mage::helper('elanding')->__('Row %d has incorrect columns count', $rowNumber);
    		return false;
    	}
		
    	$row = array();
    	foreach ($this->_columns as $i => $key) {
    		$row[$key] = $line[$i];
    	}
    	
    	if (!isset($row['identifier']) || $row['identifier'] == '') {
    		$this->_importErrors[] = Mage::helper('elanding')->__('Landing Page Identifier should be set for Row #%d', $rowNumber);
    		return false;
    	}
    	
    	if (isset($this->_existingPages[$row['identifier']])) {
    		$this->_importErrors[] = Mage::helper('elanding')->__('Record with "%s" identifier exists already. Delete it before update. Row #%d', $row['identifier'], $rowNumber);
    		return false;
    	}
    	
    	if (isset($row['attributes']) && $row['attributes'] != '' && @unserialize($row['attributes']) === FALSE) {
    		$this->_importErrors[] = Mage::helper('elanding')->__('Attributes have invalid format. Check that they can be unseralizes using PHP function unserialize. Row #%d', $rowNumber);
    		return false;
    	}
    	return $row;
    }
    
    public function loadExistingPages(){
    	$pages = Mage::getModel('elanding/page')->getCollection();
    	$array = array();
    	foreach ($pages as $page) {
    		$array[$page->getIdentifier()] = $page;
    	}
    	$this->_existingPages = $array;
    }
    
    public function saveImportData($data){
    	if (!empty($data)) {
    		$ids = array();
    		foreach ($data as $row) {
            	$this->_getWriteAdapter()->insert($this->getMainTable(), $row);            	
            	$ids[] = $this->_getWriteAdapter()->lastInsertId();
    		}

    		$stores = Mage::app()->getRequest()->getParam('groups');
            $stores = $stores['import']['fields']['store']['value'];
    		
    		$storesInsert = array();
    		foreach ($stores as $storeId) {
            	foreach ($ids as $id) {
            		$storesInsert[] = array(
            			'page_id' => $id,
            			'store_id' => $storeId
            		);
            	}
            }
                		
            $storeTable = Mage::getSingleton('core/resource')->getTableName('elanding/page_store');            
            $this->_getWriteAdapter()->insertArray($storeTable, array('page_id', 'store_id'), $storesInsert);            
        }
    }
}