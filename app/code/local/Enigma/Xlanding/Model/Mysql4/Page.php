<?php
/**
* dasENIGMA.
*
* NOTICE OF LICENSE
*
* This source file is subject to the EULA
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://codecanyon.net/licenses/regular
*
* =================================================================
*                 MAGENTO EDITION USAGE NOTICE
* =================================================================
* This package designed for Magento community edition
* dasENIGMA does not guarantee correct work of this extension
* on any other Magento edition except Magento community edition.
* dasENIGMA does not provide extension support in case of
* incorrect edition usage.
* =================================================================
*
* @category   Enigma
* @package    Enigma_LandingPages
* @version    1.0
* @copyright  Copyright (c) 2014 dasENIGMA. (http://codecanyon.net/user/dasEnigma/portfolio?ref=dasEnigma)
* @license    http://codecanyon.net/licenses/regular
*/
class Enigma_Xlanding_Model_Mysql4_Page extends Mage_Core_Model_Mysql4_Abstract{
    protected $_store  = null;

    protected function _construct(){
        $this->_init('elanding/page', 'page_id');
    }
    
    protected function _beforeDelete(Mage_Core_Model_Abstract $object){
        $condition = array(
            'page_id = ?' => (int) $object->getId(),
        );
        $this->_getWriteAdapter()->delete($this->getTable('elanding/page_store'), $condition);
        return parent::_beforeDelete($object);
    }

    protected function _beforeSave(Mage_Core_Model_Abstract $object){    	
        if (!$this->getIsUniquePageToStores($object)) {
            Mage::throwException(Mage::helper('elanding')->__('A page URL key for specified store already exists.'));
        }

        if (!$this->isValidPageIdentifier($object)) {
            Mage::throwException(Mage::helper('elanding')->__('The page URL key contains capital letters or disallowed symbols.'));
        }

        if ($this->isNumericPageIdentifier($object)) {
            Mage::throwException(Mage::helper('elanding')->__('The page URL key cannot consist only of numbers.'));
        }
        return parent::_beforeSave($object);
    }

    protected function _afterSave(Mage_Core_Model_Abstract $object){
        $oldStores = $this->lookupStoreIds($object->getId());
        $newStores = (array)$object->getStores();
        if (empty($newStores)) {
            $newStores = (array)$object->getStoreId();
        }
        $table = $this->getTable('elanding/page_store');
        $insert = array_diff($newStores, $oldStores);
        $delete = array_diff($oldStores, $newStores);

        if ($delete) {
            $where = array(
                'page_id = ?' => (int) $object->getId(),
                'store_id IN (?)' => $delete
            );
            $this->_getWriteAdapter()->delete($table, $where);
        }

        if ($insert) {
            $data = array();
            foreach ($insert as $storeId) {
                $data[] = array(
                    'page_id' => (int) $object->getId(),
                    'store_id' => (int) $storeId
                );
            }
            $this->_getWriteAdapter()->insertMultiple($table, $data);
        }
        return parent::_afterSave($object);
    }

    public function load(Mage_Core_Model_Abstract $object, $value, $field = null){
        if (!is_numeric($value) && is_null($field)) {
            $field = 'identifier';
        }
        return parent::load($object, $value, $field);
    }

    protected function _afterLoad(Mage_Core_Model_Abstract $object){
        if ($object->getId()) {
            $stores = $this->lookupStoreIds($object->getId());
            $object->setData('store_id', $stores);
        }
        return parent::_afterLoad($object);
    }
	
    protected function _getLoadSelect($field, $value, $object){
        $select = parent::_getLoadSelect($field, $value, $object);
        if ($object->getStoreId()) {
            $storeIds = array(Mage_Core_Model_App::ADMIN_STORE_ID, (int)$object->getStoreId());
            $select->join(
                array('elanding_page_store' => $this->getTable('elanding/page_store')),
                $this->getMainTable() . '.page_id = cms_page_store.page_id',
                array())
                ->where('is_active = ?', 1)
                ->where('elanding_page_store.store_id IN (?)', $storeIds)
                ->order('elanding_page_store.store_id DESC')
                ->limit(1);
        }
        return $select;
    }
	
    protected function _getLoadByIdentifierSelect($identifier, $store, $isActive = null){
        $select = $this->_getReadAdapter()->select()
            ->from(array('cp' => $this->getMainTable()))
            ->join(
                array('cps' => $this->getTable('elanding/page_store')),
                'cp.page_id = cps.page_id',
                array())
            ->where('cp.identifier = ?', $identifier)
            ->where('cps.store_id IN (?)', $store);
        if (!is_null($isActive)) {
            $select->where('cp.is_active = ?', $isActive);
        }
        return $select;
    }
	
    public function getIsUniquePageToStores(Mage_Core_Model_Abstract $object){
        if (Mage::app()->isSingleStoreMode() || !$object->hasStores()) {
            $stores = array(Mage_Core_Model_App::ADMIN_STORE_ID);
        } else {
            $stores = (array)$object->getData('stores');
        }

        $select = $this->_getLoadByIdentifierSelect($object->getData('identifier'), $stores);

        if ($object->getId()) {
            $select->where('cps.page_id <> ?', $object->getId());
        }

        if ($this->_getWriteAdapter()->fetchRow($select)) {
            return false;
        }
        return true;
    }

    protected function isNumericPageIdentifier(Mage_Core_Model_Abstract $object){
        return preg_match('/^[0-9]+$/', $object->getData('identifier'));
    }

    protected function isValidPageIdentifier(Mage_Core_Model_Abstract $object){
        return preg_match('/^[a-z0-9][a-z0-9_\/-]+(\.[a-z0-9_-]+)?$/', $object->getData('identifier'));
    }
	
    public function checkIdentifier($identifier, $storeId){
        $stores = array(Mage_Core_Model_App::ADMIN_STORE_ID, $storeId);
        $select = $this->_getLoadByIdentifierSelect($identifier, $stores, 1);
        $select->reset(Zend_Db_Select::COLUMNS)
            ->columns('cp.page_id')
            ->order('cps.store_id DESC')
            ->limit(1);
        return $this->_getReadAdapter()->fetchOne($select);
    }
	
    public function lookupStoreIds($pageId){
        $adapter = $this->_getReadAdapter();
        $select = $adapter->select()
            ->from($this->getTable('elanding/page_store'), 'store_id')
            ->where('page_id = ?',(int)$pageId);
        return $adapter->fetchCol($select);
    }
	
    public function setStore($store){
        $this->_store = $store;
        return $this;
    }
	
    public function getStore(){
        return Mage::app()->getStore($this->_store);
    }
    
    public function massChangeStatus($ids, $status){
        $db = $this->_getWriteAdapter();
        $db->update($this->getMainTable(),array('is_active' => $status), 'page_id IN(' . implode(',', $ids) . ') ');            
        return true;
    }    
}