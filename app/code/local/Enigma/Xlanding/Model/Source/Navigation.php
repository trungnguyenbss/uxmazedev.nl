<?php
/**
* dasENIGMA.
*
* NOTICE OF LICENSE
*
* This source file is subject to the EULA
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://codecanyon.net/licenses/regular
*
* =================================================================
*                 MAGENTO EDITION USAGE NOTICE
* =================================================================
* This package designed for Magento community edition
* dasENIGMA does not guarantee correct work of this extension
* on any other Magento edition except Magento community edition.
* dasENIGMA does not provide extension support in case of
* incorrect edition usage.
* =================================================================
*
* @category   Enigma
* @package    Enigma_LandingPages
* @version    1.0
* @copyright  Copyright (c) 2014 dasENIGMA. (http://codecanyon.net/user/dasEnigma/portfolio?ref=dasEnigma)
* @license    http://codecanyon.net/licenses/regular
*/
class Enigma_Xlanding_Model_Source_Navigation extends Varien_Object{
	const INCLUDE_NO = 0;
	const INCLUDE_LEFT = 'left';
	const INCLUDE_RIGHT = 'right';
	
	public function toOptionArray(){
	    $hlp = Mage::helper('elanding');
		return array(
			array('value' => self::INCLUDE_NO, 'label' => $hlp->__('No')),
			array('value' => self::INCLUDE_LEFT, 'label' => $hlp->__('Yes, Left Sidebar')),
			array('value' => self::INCLUDE_RIGHT, 'label' => $hlp->__('Yes, Right Sidebar')),
		);
	}
	
	public function toFlatArray(){
	    $hlp = Mage::helper('elanding');
		return array(
			self::INCLUDE_NO => $hlp->__('No'),
			self::INCLUDE_LEFT =>  $hlp->__('Yes, Left Sidebar'),
			self::INCLUDE_RIGHT => $hlp->__('Yes, Right Sidebar')
		);
	}	
}