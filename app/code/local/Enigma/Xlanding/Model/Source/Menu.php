<?php
/**
* dasENIGMA.
*
* NOTICE OF LICENSE
*
* This source file is subject to the EULA
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://codecanyon.net/licenses/regular
*
* =================================================================
*                 MAGENTO EDITION USAGE NOTICE
* =================================================================
* This package designed for Magento community edition
* dasENIGMA does not guarantee correct work of this extension
* on any other Magento edition except Magento community edition.
* dasENIGMA does not provide extension support in case of
* incorrect edition usage.
* =================================================================
*
* @category   Enigma
* @package    Enigma_LandingPages
* @version    1.0
* @copyright  Copyright (c) 2014 dasENIGMA. (http://codecanyon.net/user/dasEnigma/portfolio?ref=dasEnigma)
* @license    http://codecanyon.net/licenses/regular
*/
class Enigma_Xlanding_Model_Source_Menu extends Varien_Object{
	const INCLUDE_NO = 0;
	const INCLUDE_APPEND = 1;
	const INCLUDE_PREPEND = 2;
	
	public static function toOptionArray(){
	    $hlp = Mage::helper('elanding');
		return array(
			array('value' => self::INCLUDE_NO, 'label' => $hlp->__('No')),
			array('value' => self::INCLUDE_APPEND, 'label' => $hlp->__('Yes, Append to existing')),
			array('value' => self::INCLUDE_PREPEND, 'label' => $hlp->__('Yes, Prepend existing')),
		);
	}	
}