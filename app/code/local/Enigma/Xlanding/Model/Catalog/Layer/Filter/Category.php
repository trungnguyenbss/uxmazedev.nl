<?php
/**
* dasENIGMA.
*
* NOTICE OF LICENSE
*
* This source file is subject to the EULA
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://codecanyon.net/licenses/regular
*
* =================================================================
*                 MAGENTO EDITION USAGE NOTICE
* =================================================================
* This package designed for Magento community edition
* dasENIGMA does not guarantee correct work of this extension
* on any other Magento edition except Magento community edition.
* dasENIGMA does not provide extension support in case of
* incorrect edition usage.
* =================================================================
*
* @category   Enigma
* @package    Enigma_LandingPages
* @version    1.0
* @copyright  Copyright (c) 2014 dasENIGMA. (http://codecanyon.net/user/dasEnigma/portfolio?ref=dasEnigma)
* @license    http://codecanyon.net/licenses/regular
*/
class Enigma_Xlanding_Model_Catalog_Layer_Filter_Category extends Enigma_Xlanding_Model_Catalog_Layer_Filter_Category_Pure{
    protected function _initItems(){
    	if (!$key = Mage::app()->getRequest()->getParam('e_landing')) {
    		return parent::_initItems();
    	}
    	
        $data  = $this->_getItemsData();
        $items = array();
        foreach ($data as $itemData) {
            if (!$itemData)
                continue;
                
            $obj = new Varien_Object();
            $obj->setData($itemData);
            if (isset($itemData['id'])) {
            	$url = Mage::helper('eshopby/url')->getFullUrl();
            	if (strpos($url, '?') !== false) {
            		$url .= '&cat=' . $itemData['id'];
            	} else {
            		$url .= '?cat=' . $itemData['id'];
            	}
            } else {
            	$url = Mage::helper('elanding/url')->getLandingUrl(array('cat' => $itemData['value']));
            }
            $obj->setUrl($url);

            $items[] = $obj;
        }
        $this->_items = $items;
        return $this;
    }
}