<?php
/**
* dasENIGMA.
*
* NOTICE OF LICENSE
*
* This source file is subject to the EULA
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://codecanyon.net/licenses/regular
*
* =================================================================
*                 MAGENTO EDITION USAGE NOTICE
* =================================================================
* This package designed for Magento community edition
* dasENIGMA does not guarantee correct work of this extension
* on any other Magento edition except Magento community edition.
* dasENIGMA does not provide extension support in case of
* incorrect edition usage.
* =================================================================
*
* @category   Enigma
* @package    Enigma_LandingPages
* @version    1.0
* @copyright  Copyright (c) 2014 dasENIGMA. (http://codecanyon.net/user/dasEnigma/portfolio?ref=dasEnigma)
* @license    http://codecanyon.net/licenses/regular
*/
class Enigma_Xlanding_Model_Catalog_Layer_Filter_Item extends Enigma_Xlanding_Model_Catalog_Layer_Filter_Item_Amasty_Pure{
    public function getUrl(){
        if ($key = Mage::app()->getRequest()->getParam('e_landing')) {
			if (!Mage::helper('elanding')->seoLinksActive()) {
				return Mage::helper('elanding/url')->getLandingUrl(array($this->getFilter()->getRequestVar() => $this->getValue()));
			}
        }
        return parent::getUrl();
    }
    
	public function getRemoveUrl(){
    	if ($key = Mage::app()->getRequest()->getParam('e_landing')) {
        	$exclude = array(
        		$this->getFilter()->getRequestVar()
        	);
        	if (!Mage::helper('elanding')->seoLinksActive()) {
        		return Mage::helper('elanding/url')->getLandingUrl(null, $exclude);
        	}
        }
        return parent::getRemoveUrl();                
    }
}