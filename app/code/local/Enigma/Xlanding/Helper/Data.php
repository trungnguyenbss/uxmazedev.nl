<?php
/**
* dasENIGMA.
*
* NOTICE OF LICENSE
*
* This source file is subject to the EULA
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://codecanyon.net/licenses/regular
*
* =================================================================
*                 MAGENTO EDITION USAGE NOTICE
* =================================================================
* This package designed for Magento community edition
* dasENIGMA does not guarantee correct work of this extension
* on any other Magento edition except Magento community edition.
* dasENIGMA does not provide extension support in case of
* incorrect edition usage.
* =================================================================
*
* @category   Enigma
* @package    Enigma_LandingPages
* @version    1.0
* @copyright  Copyright (c) 2014 dasENIGMA. (http://codecanyon.net/user/dasEnigma/portfolio?ref=dasEnigma)
* @license    http://codecanyon.net/licenses/regular
*/
class Enigma_Xlanding_Helper_Data extends Mage_Core_Helper_Abstract{
	const STATUS_ENABLED = 1;
	const STATUS_DISABLED = 0;
	
	public function getAvailableStatuses(){
		return array(
			self::STATUS_ENABLED => Mage::helper('elanding')->__('Enabled'),
            self::STATUS_DISABLED => Mage::helper('elanding')->__('Disabled'),
		);
	}
	
	public function isPrice($attributeCode){
		return in_array($attributeCode, array('price'));
	}
	
	public function getOperations($filterable = false, $attributeCode = null){
        $res = array();        
        if ($filterable && !$this->isPrice($attributeCode)) {
        	$res['eq']    = $this->__('equal');
	        $res['in']    = $this->__('is one of');
	        $res['nin']   = $this->__('is not one of');
        } else {
	        $res['eq']    = $this->__('equal');
	        $res['neq']   = $this->__('not equal');
	        $res['gt']    = $this->__('greater than');
	        $res['lt']    = $this->__('less than');
	        $res['gteq']  = $this->__('equal or greater than');
	        $res['lteq']  = $this->__('equal or less than');
	        $res['in']    = $this->__('is one of');
	        $res['nin']   = $this->__('is not one of');
	        $res['like']  = $this->__('contains');
	        $res['nlike'] = $this->__('not contains');	
        }
        return $res;
    }
	
	public function getMenuPositions(){
		return array(
			Enigma_Xlanding_Model_Source_Menu::INCLUDE_NO => Mage::helper('elanding')->__('No'),
			Enigma_Xlanding_Model_Source_Menu::INCLUDE_APPEND => Mage::helper('elanding')->__('Yes, Append to existing'),
			Enigma_Xlanding_Model_Source_Menu::INCLUDE_PREPEND => Mage::helper('elanding')->__('Yes, Prepend existing'),
		);
	}
	
	public function getColumnCount(){
		return Mage::getStoreConfig('elanding/advanced/column_count');
	}
	
	public function newFilterActive(){
		return (Mage::app()->getRequest()->getParam('e_landing') && Mage::getStoreConfig('elanding/advanced/new_criteria')
			&& Mage::registry('elanding_page')->getIsNew() != 0 && !$this->isVersionLessThan(1,7));
	}
	
	public function seoLinksActive(){
           if ('true' !== (string)Mage::getConfig()->getNode('modules/Enigma_Shopby/active')) return false;
	   return class_exists('Enigma_Shopby_Block_Catalog_Layer_View') && Mage::getStoreConfig('eshopby/seo/urls');
	}
	
	public function isVersionLessThan($major=5, $minor=3){
        $curr = explode('.', Mage::getVersion());
        $need = func_get_args();
        foreach ($need as $k => $v){
            if ($curr[$k] != $v)
                return ($curr[$k] < $v);
        }
        return false;
    }
}