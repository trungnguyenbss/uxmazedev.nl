<?php
/**
* dasENIGMA.
*
* NOTICE OF LICENSE
*
* This source file is subject to the EULA
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://codecanyon.net/licenses/regular
*
* =================================================================
*                 MAGENTO EDITION USAGE NOTICE
* =================================================================
* This package designed for Magento community edition
* dasENIGMA does not guarantee correct work of this extension
* on any other Magento edition except Magento community edition.
* dasENIGMA does not provide extension support in case of
* incorrect edition usage.
* =================================================================
*
* @category   Enigma
* @package    Enigma_LandingPages
* @version    1.0
* @copyright  Copyright (c) 2014 dasENIGMA. (http://codecanyon.net/user/dasEnigma/portfolio?ref=dasEnigma)
* @license    http://codecanyon.net/licenses/regular
*/
class Enigma_Xlanding_Helper_Url extends Mage_Core_Helper_Abstract{
	public function getLandingUrl($query = null, $exclude = array()){
		if (!$query) {
			$query = Mage::app()->getRequest()->getParams();
		}
		
		$query = array_merge(Mage::app()->getRequest()->getParams(), $query);
		
		foreach ($exclude as $excludeKey) {
			if (isset($query[$excludeKey])) {
				unset($query[$excludeKey]);
			}
		}
		unset($query['page_id']);
		unset($query['e_landing']);
		
        $key = Mage::app()->getRequest()->getParam('e_landing');
        if (count($query) > 0) {
            $query = '?' . http_build_query($query);
        } else {
            $query = '';
        }
        $suffix = Mage::getStoreConfig('catalog/seo/category_url_suffix');        
        return Mage::getBaseUrl() . $key . $suffix . $query;
	}
	
	public function getClearUrl(){
	    $key = Mage::app()->getRequest()->getParam('e_landing');
	    $suffix = Mage::getStoreConfig('catalog/seo/category_url_suffix');	    
       	return Mage::getBaseUrl() . $key . $suffix;
	}
}