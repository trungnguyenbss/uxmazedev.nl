<?php
/**
* dasENIGMA.
*
* NOTICE OF LICENSE
*
* This source file is subject to the EULA
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://codecanyon.net/licenses/regular
*
* =================================================================
*                 MAGENTO EDITION USAGE NOTICE
* =================================================================
* This package designed for Magento community edition
* dasENIGMA does not guarantee correct work of this extension
* on any other Magento edition except Magento community edition.
* dasENIGMA does not provide extension support in case of
* incorrect edition usage.
* =================================================================
*
* @category   Enigma
* @package    Enigma_LandingPages
* @version    1.0
* @copyright  Copyright (c) 2014 dasENIGMA. (http://codecanyon.net/user/dasEnigma/portfolio?ref=dasEnigma)
* @license    http://codecanyon.net/licenses/regular
*/
$this->startSetup();
$pagesData = $this->getConnection()->fetchAll("SELECT page_id, attributes FROM {$this->getTable('elanding/page')}");
   foreach ($pagesData as $page) {
    	$pageId = $page['page_id'];
    	$attributes = $page['attributes'];
    	
    	if (empty($attributes)) {
    		continue;
    	}
		
    	$deserialize = unserialize($attributes);
    	
    	$newFormat = array();
    	foreach ($deserialize as $code => $options) {
    		foreach ($options as $option) {
    			$newFormat[] = array(
    				'code' => $code,
    				'value' => $option,
    				'cond' => 'eq'
    			);	
    		}
    	}
    	
    	$serialize = serialize($newFormat);    	
        $query = "UPDATE `{$this->getTable('elanding/page')}` SET `attributes` = '" . $serialize . "' WHERE `page_id` = $pageId;";        
		$this->run($query);
    }
$this->endSetup();