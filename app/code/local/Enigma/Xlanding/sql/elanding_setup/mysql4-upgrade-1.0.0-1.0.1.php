<?php
/**
* dasENIGMA.
*
* NOTICE OF LICENSE
*
* This source file is subject to the EULA
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://codecanyon.net/licenses/regular
*
* =================================================================
*                 MAGENTO EDITION USAGE NOTICE
* =================================================================
* This package designed for Magento community edition
* dasENIGMA does not guarantee correct work of this extension
* on any other Magento edition except Magento community edition.
* dasENIGMA does not provide extension support in case of
* incorrect edition usage.
* =================================================================
*
* @category   Enigma
* @package    Enigma_LandingPages
* @version    1.0
* @copyright  Copyright (c) 2014 dasENIGMA. (http://codecanyon.net/user/dasEnigma/portfolio?ref=dasEnigma)
* @license    http://codecanyon.net/licenses/regular
*/
$this->startSetup();
$this->run("
    ALTER TABLE `{$this->getTable('elanding/page')}` ADD `layout_static_top` VARCHAR(255) DEFAULT NULL;
    ALTER TABLE `{$this->getTable('elanding/page')}` ADD `layout_static_bottom` VARCHAR(255) DEFAULT NULL;
    ALTER TABLE `{$this->getTable('elanding/page')}` CHANGE COLUMN `attr_code` `attributes` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '',
 	DROP COLUMN `attr_value`;
");
$this->endSetup();