<?php
/**
* dasENIGMA.
*
* NOTICE OF LICENSE
*
* This source file is subject to the EULA
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://codecanyon.net/licenses/regular
*
* =================================================================
*                 MAGENTO EDITION USAGE NOTICE
* =================================================================
* This package designed for Magento community edition
* dasENIGMA does not guarantee correct work of this extension
* on any other Magento edition except Magento community edition.
* dasENIGMA does not provide extension support in case of
* incorrect edition usage.
* =================================================================
*
* @category   Enigma
* @package    Enigma_LandingPages
* @version    1.0
* @copyright  Copyright (c) 2014 dasENIGMA. (http://codecanyon.net/user/dasEnigma/portfolio?ref=dasEnigma)
* @license    http://codecanyon.net/licenses/regular
*/
$this->startSetup();
$this->run("
CREATE TABLE  `{$this->getTable('elanding/page')}` (
  `page_id` smallint(6) NOT NULL AUTO_INCREMENT COMMENT 'Page ID',
  `title` varchar(255) DEFAULT NULL COMMENT 'Page Title',
  `root_template` varchar(255) DEFAULT NULL COMMENT 'Page Template',
  `meta_title` varchar(255) NOT NULL DEFAULT '',
  `meta_keywords` text COMMENT 'Page Meta Keywords',
  `meta_description` text COMMENT 'Page Meta Description',
  `identifier` varchar(100) NOT NULL DEFAULT '' COMMENT 'Page String Identifier',
  `is_active` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Is Page Active',
  `layout_update_xml` text COMMENT 'Page Layout Update Content',
  `is_new` tinyint(1) NOT NULL,
  `is_sale` tinyint(1) NOT NULL,
  `include_type` tinyint(1) NOT NULL,
  `include_sku` text,
  `category` mediumint(9) NOT NULL,
  `attr_code` varchar(255) NOT NULL,
  `attr_value` varchar(255) NOT NULL,
  `stock_less` int(11) NOT NULL,
  `stock_more` int(11) NOT NULL,
  `stock_status` tinyint(4) NOT NULL,
  PRIMARY KEY (`page_id`),
  KEY `IDX_E_LANDING_PAGE_IDENTIFIER` (`identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Enigma Landing Page Table';

CREATE TABLE  `{$this->getTable('elanding/page_store')}` (
  `page_id` smallint(6) NOT NULL COMMENT 'Page ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  PRIMARY KEY (`page_id`,`store_id`),
  KEY `IDX_E_LANDING_PAGE_STORE_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
$this->endSetup();