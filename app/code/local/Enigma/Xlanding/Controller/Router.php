<?php
/**
* dasENIGMA.
*
* NOTICE OF LICENSE
*
* This source file is subject to the EULA
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://codecanyon.net/licenses/regular
*
* =================================================================
*                 MAGENTO EDITION USAGE NOTICE
* =================================================================
* This package designed for Magento community edition
* dasENIGMA does not guarantee correct work of this extension
* on any other Magento edition except Magento community edition.
* dasENIGMA does not provide extension support in case of
* incorrect edition usage.
* =================================================================
*
* @category   Enigma
* @package    Enigma_LandingPages
* @version    1.0
* @copyright  Copyright (c) 2014 dasENIGMA. (http://codecanyon.net/user/dasEnigma/portfolio?ref=dasEnigma)
* @license    http://codecanyon.net/licenses/regular
*/
class Enigma_Xlanding_Controller_Router extends Mage_Core_Controller_Varien_Router_Abstract{
    public function initControllerRouters($observer){
        $front = $observer->getEvent()->getFront();
        $front->addRouter('elanding', $this);
    }
	
    public function match(Zend_Controller_Request_Http $request){
     	if (Mage::app()->getStore()->isAdmin()) {
            return false;
        }
        
    	$pathInfo = $request->getPathInfo();
        $suffix = Mage::getStoreConfig('catalog/seo/category_url_suffix');
        if ($suffix && '/' != $suffix){
            $pathInfo = str_replace($suffix, '', $pathInfo);
        }
        
        $pathInfo = explode('/', trim($pathInfo, '/ '), 2);
        $identifier = $pathInfo[0];
        $params = (isset($pathInfo[1]) ? $pathInfo[1] : '');
		
        $page = Mage::getModel('elanding/page');
        $pageId = $page->checkIdentifier($identifier, Mage::app()->getStore()->getId());
        if (!$pageId) {
            return false;
        }
		
        $params = trim($params, '/ ');
        if ($params){
            $params = explode('/', $params);
            Mage::register('amshopby_current_params', $params);
        }

        $request->setModuleName('elanding')
            ->setControllerName('page')
            ->setActionName('view')
            ->setParam('page_id', $pageId)
            ->setParam('e_landing', $identifier);            
        return true;
    }
}