<?php
/**
* dasENIGMA.
*
* NOTICE OF LICENSE
*
* This source file is subject to the EULA
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://codecanyon.net/licenses/regular
*
* =================================================================
*                 MAGENTO EDITION USAGE NOTICE
* =================================================================
* This package designed for Magento community edition
* dasENIGMA does not guarantee correct work of this extension
* on any other Magento edition except Magento community edition.
* dasENIGMA does not provide extension support in case of
* incorrect edition usage.
* =================================================================
*
* @category   Enigma
* @package    Enigma_LandingPages
* @version    1.0
* @copyright  Copyright (c) 2014 dasENIGMA. (http://codecanyon.net/user/dasEnigma/portfolio?ref=dasEnigma)
* @license    http://codecanyon.net/licenses/regular
*/
class Enigma_Xlanding_Block_Adminhtml_Page_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs{
    public function __construct(){
        parent::__construct();
        $this->setId('ruleTabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('elanding')->__('Landing Page'));
    }

    protected function _beforeToHtml(){
        $tabs = array(            
			'main' => 'General',
			'meta' => 'Meta',
            'design' => 'Design',
        	'condition' => 'Conditions'
        );
        
        foreach ($tabs as $code => $label){
            $label = Mage::helper('elanding')->__($label);
            $content = $this->getLayout()->createBlock('elanding/adminhtml_page_edit_tab_' . $code)->setTitle($label)->toHtml();                
            $this->addTab($code, array(
                'label' => $label,
                'content' => $content,
            ));
        }        
        return parent::_beforeToHtml();
    }
}