<?php
/**
* dasENIGMA.
*
* NOTICE OF LICENSE
*
* This source file is subject to the EULA
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://codecanyon.net/licenses/regular
*
* =================================================================
*                 MAGENTO EDITION USAGE NOTICE
* =================================================================
* This package designed for Magento community edition
* dasENIGMA does not guarantee correct work of this extension
* on any other Magento edition except Magento community edition.
* dasENIGMA does not provide extension support in case of
* incorrect edition usage.
* =================================================================
*
* @category   Enigma
* @package    Enigma_LandingPages
* @version    1.0
* @copyright  Copyright (c) 2014 dasENIGMA. (http://codecanyon.net/user/dasEnigma/portfolio?ref=dasEnigma)
* @license    http://codecanyon.net/licenses/regular
*/
class Enigma_Xlanding_Block_Adminhtml_Page_Edit extends Mage_Adminhtml_Block_Widget_Form_Container{
    public function __construct(){
        parent::__construct();
                 
        $this->_objectId = 'id'; 
        $this->_blockGroup = 'elanding';
        $this->_controller = 'adminhtml_page';
        
        $this->_addButton('save_and_continue', array(
                'label'     => Mage::helper('elanding')->__('Save and Continue Edit'),
                'onclick'   => 'saveAndContinueEdit()',
                'class' => 'save'
            ), 10);
        $this->_formScripts[] = "var landingConditionTmpl = '';";
        $this->_formScripts[] = "function saveAndContinueEdit(){ editForm.submit($('edit_form').action + 'continue/edit') }";
        $this->_formScripts[] = "function landingNewField(){ 
        	if ($('attributes')) { 
        		var tr = $('attributes').down('div').down('table').down('tbody').down('tr:last-child');
        		
        		/* Is New */ 
        		if (!tr) {
        			$('attributes').down('div').down('table').down('tbody').update(landingConditionTmpl);
				} else {
					tr.insert({'after': landingConditionTmpl});
				}
				
    		} 
    	}";
        $this->_formScripts[] = "function landingRemove(element) { 
        	if ($(element)) { 
        		$(element).up().up().next('tr').remove(); $(element).up().up().remove();
			} 
    	}
		";        
		$this->_formScripts[] = " function showCond(sel) {
            new Ajax.Request('" . $this->getUrl('adminhtml/page/options', array('isAjax'=>true)) ."', {
                parameters: {code : sel.value},
                onSuccess: function(transport) {
                    $(sel).next('span').update(transport.responseText);
                }
            });
        }";
		$this->_formScripts[] = " function showOptions(sel, c) {
            new Ajax.Request('" . $this->getUrl('adminhtml/page/options', array('isAjax'=>true)) ."', {
                parameters: {cond : sel.value, code: c},
                onSuccess: function(transport) {
                	$(sel).up().next('span').update(transport.responseText);
                }
            });
        }";
    }

    public function getHeaderText(){
        $header = Mage::helper('elanding')->__('New Landing Page');
        $model = Mage::registry('elanding_page');
        if ($model->getId()){
            $header = Mage::helper('elanding')->__('Edit Landing Page `%s`', $model->getTitle());
        }
        return $header;
    }
}