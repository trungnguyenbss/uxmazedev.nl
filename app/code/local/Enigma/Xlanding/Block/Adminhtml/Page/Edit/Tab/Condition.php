<?php
/**
* dasENIGMA.
*
* NOTICE OF LICENSE
*
* This source file is subject to the EULA
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://codecanyon.net/licenses/regular
*
* =================================================================
*                 MAGENTO EDITION USAGE NOTICE
* =================================================================
* This package designed for Magento community edition
* dasENIGMA does not guarantee correct work of this extension
* on any other Magento edition except Magento community edition.
* dasENIGMA does not provide extension support in case of
* incorrect edition usage.
* =================================================================
*
* @category   Enigma
* @package    Enigma_LandingPages
* @version    1.0
* @copyright  Copyright (c) 2014 dasENIGMA. (http://codecanyon.net/user/dasEnigma/portfolio?ref=dasEnigma)
* @license    http://codecanyon.net/licenses/regular
*/
class Enigma_Xlanding_Block_Adminhtml_Page_Edit_Tab_Condition extends Enigma_Xlanding_Block_Adminhtml_Widget_Edit_Tab_Dynamic{
 	public function __construct(){
        parent::__construct();
        $this->setTemplate('elanding/page/condition.phtml');        
        $this->_model = 'elanding_page';
    } 

    public function getPageAttributes(){
    	return Mage::registry('elanding_page')->getAttributesAsArray();
    }
    
    protected function getAttributes(){
        $collection = Mage::getResourceModel('eav/entity_attribute_collection')
            ->setItemObjectClass('catalog/resource_eav_attribute')
            ->setEntityTypeFilter(Mage::getResourceModel('catalog/product')->getTypeId());
            
        $options = array('' => '');
		foreach ($collection as $attribute){
		    $label = $attribute->getFrontendLabel();
			if ($label){
			    $options[$attribute->getAttributeCode()] = $label;
			}
		}
		asort($options);        
		return $options;
    }
	
    protected function getTree(){
        $rootId = Mage::app()->getStore(0)->getRootCategoryId();         
        $tree = array();
        
        $collection = Mage::getModel('catalog/category')->getCollection()->addNameToResult();
        
        $pos = array();
        foreach ($collection as $cat){
            $path = explode('/', $cat->getPath());
            if ((!$rootId || in_array($rootId, $path)) && $cat->getLevel()){
                $tree[$cat->getId()] = array(
                    'label' => str_repeat('--', $cat->getLevel()) . $cat->getName(), 
                    'value' => $cat->getId(),
                    'path'  => $path,
                );
            }
            $pos[$cat->getId()] = $cat->getPosition();
        }
        
        foreach ($tree as $catId => $cat){
            $order = array();
            foreach ($cat['path'] as $id){
            	if (isset($pos[$id])){
                	$order[] = $pos[$id];
            	}
            }
            $tree[$catId]['order'] = $order;
        }
        
        usort($tree, array($this, 'compare'));
        array_unshift($tree, array('value'=>'', 'label'=>''));        
        return $tree;
    }
	
    public function compare($a, $b){
        foreach ($a['path'] as $i => $id){
            if (!isset($b['path'][$i])){
                return 1;
            }
            if ($id != $b['path'][$i]){
                $p = isset($a['order'][$i]) ? $a['order'][$i] : 0;
                $p2 = isset($b['order'][$i]) ? $b['order'][$i] : 0;
                return ($p < $p2) ? -1 : 1;
            }
        }
        return ($a['value'] == $b['value']) ? 0 : -1;
    }
}