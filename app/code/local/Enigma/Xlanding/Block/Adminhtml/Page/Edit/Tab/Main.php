<?php
/**
* dasENIGMA.
*
* NOTICE OF LICENSE
*
* This source file is subject to the EULA
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://codecanyon.net/licenses/regular
*
* =================================================================
*                 MAGENTO EDITION USAGE NOTICE
* =================================================================
* This package designed for Magento community edition
* dasENIGMA does not guarantee correct work of this extension
* on any other Magento edition except Magento community edition.
* dasENIGMA does not provide extension support in case of
* incorrect edition usage.
* =================================================================
*
* @category   Enigma
* @package    Enigma_LandingPages
* @version    1.0
* @copyright  Copyright (c) 2014 dasENIGMA. (http://codecanyon.net/user/dasEnigma/portfolio?ref=dasEnigma)
* @license    http://codecanyon.net/licenses/regular
*/
class Enigma_Xlanding_Block_Adminhtml_Page_Edit_Tab_Main extends Mage_Adminhtml_Block_Widget_Form{
    protected function _prepareForm(){
        $model = Mage::registry('elanding_page');
        $helper = Mage::helper('elanding');
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('page_');
        $fieldset = $form->addFieldset('base_fieldset', array('legend'=>$helper->__('Page Information')));

        if ($model->getPageId()) {
            $fieldset->addField('page_id', 'hidden', array(
                'name' => 'page_id',
            ));
        }

        $fieldset->addField('title', 'text', array(
            'name' => 'title',
            'label' => $helper->__('Page Name'),
            'title' => $helper->__('Page Name'),
            'required' => true,
        ));

        $fieldset->addField('identifier', 'text', array(
            'name' => 'identifier',
            'label' => $helper->__('URL Key'),
            'title' => $helper->__('URL Key'),
            'required' => true,
            'class' => 'validate-identifier',
            'note' => $helper->__('Relative to Website Base URL'),
        ));
		
        if (!Mage::app()->isSingleStoreMode()) {
            $fieldset->addField('store_id', 'multiselect', array(
				'label' => $helper->__('Stores'),
				'name' => 'stores[]',
				'values' => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm()            
			));  
        }
        else {
            $fieldset->addField('store_id', 'hidden', array(
                'name' => 'stores[]',
                'value' => Mage::app()->getStore(true)->getId()
            ));
            $model->setStoreId(Mage::app()->getStore(true)->getId());
        }

        $fieldset->addField('is_active', 'select', array(
            'label' => $helper->__('Status'),
            'title' => $helper->__('Page Status'),
            'name' => 'is_active',
            'required' => true,
            'options' => $helper->getAvailableStatuses()
        ));

        $form->setValues($model->getData());
        $this->setForm($form);
        return parent::_prepareForm();
    }
}