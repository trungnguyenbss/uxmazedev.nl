<?php
/**
* dasENIGMA.
*
* NOTICE OF LICENSE
*
* This source file is subject to the EULA
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://codecanyon.net/licenses/regular
*
* =================================================================
*                 MAGENTO EDITION USAGE NOTICE
* =================================================================
* This package designed for Magento community edition
* dasENIGMA does not guarantee correct work of this extension
* on any other Magento edition except Magento community edition.
* dasENIGMA does not provide extension support in case of
* incorrect edition usage.
* =================================================================
*
* @category   Enigma
* @package    Enigma_LandingPages
* @version    1.0
* @copyright  Copyright (c) 2014 dasENIGMA. (http://codecanyon.net/user/dasEnigma/portfolio?ref=dasEnigma)
* @license    http://codecanyon.net/licenses/regular
*/
class Enigma_Xlanding_Block_Adminhtml_Page_Edit_Tab_Design extends Mage_Adminhtml_Block_Widget_Form implements Mage_Adminhtml_Block_Widget_Tab_Interface{
    public function __construct(){
        parent::__construct();
        $this->setShowGlobalIcon(true);
    }

    protected function _prepareForm(){
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('page_');
        $model = Mage::registry('elanding_page');
        $helper = Mage::helper('elanding');
        $fieldset = $form->addFieldset('page_design', array(
            'legend' => Mage::helper('elanding')->__('Page Design'),
        ));
        
        $fieldset->addField('columns_count', 'text', array(
            'name' => 'columns_count',
            'label' => $helper->__('Columns Count'),
        	'note' => $helper->__('Count of columns in products grid')
        ));
        
        $fieldset->addField('include_navigation', 'select', array(
            'name' => 'include_navigation',
            'label' => $helper->__('Include Navigation'),
            'values' => Mage::getSingleton('elanding/source_navigation')->toFlatArray(),
        ));
        
    	$options = Mage::getResourceModel('cms/block_collection')->load();
		$identifiersList = array();
		$identifiersList[] = array(
			'value' => '',
			'label' => Mage::helper('elanding')->__('Please select a static block ...')
		);
		foreach ($options as $option) {
			$identifiersList[] = array(
				'value' => $option->getIdentifier(),
				'label' => $option->getTitle()
			);
		}
        
        $fieldset->addField('layout_static_top', 'select', array(
            'name' => 'layout_static_top',
            'label' => Mage::helper('elanding')->__('Top Static Block'),
            'values' => $identifiersList,
        	'note' => Mage::helper('elanding')->__('Choose Static Block to show Above Products List'),
        ));
        
        $fieldset->addField('layout_static_bottom', 'select', array(
            'name' => 'layout_static_bottom',
            'label' => Mage::helper('elanding')->__('Bottom Static Block'),
            'values' => $identifiersList,
         	'note' => Mage::helper('elanding')->__('Choose Static Block to show Below Products List'),
        ));
		
        $layoutFieldset = $form->addFieldset('layout_fieldset', array(
            'legend' => Mage::helper('elanding')->__('Page Layout'),
            'class' => 'fieldset-wide',
        ));
		
        $layoutFieldset->addField('root_template', 'select', array(
            'name' => 'root_template',
            'label' => Mage::helper('elanding')->__('Layout'),
            'required' => true,
            'values' => Mage::getSingleton('page/source_layout')->toOptionArray(),
        ));
        if (!$model->getId()) {
            $model->setRootTemplate(Mage::getSingleton('page/source_layout')->getDefaultValue());
        }

        $layoutFieldset->addField('layout_update_xml', 'textarea', array(
            'name' => 'layout_update_xml',
            'label' => Mage::helper('elanding')->__('Layout Update XML'),
            'style' => 'height:24em;',
        ));
        
        $form->setValues($model->getData());
        $this->setForm($form);
        return parent::_prepareForm();
    }
	
    public function getTabLabel(){
        return Mage::helper('elanding')->__('Design');
    }
	
    public function getTabTitle(){
        return Mage::helper('elanding')->__('Design');
    }
	
    public function canShowTab(){
        return true;
    }
	
    public function isHidden(){
        return false;
    }
}