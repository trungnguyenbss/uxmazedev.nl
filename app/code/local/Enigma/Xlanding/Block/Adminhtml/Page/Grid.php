<?php
/**
* dasENIGMA.
*
* NOTICE OF LICENSE
*
* This source file is subject to the EULA
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://codecanyon.net/licenses/regular
*
* =================================================================
*                 MAGENTO EDITION USAGE NOTICE
* =================================================================
* This package designed for Magento community edition
* dasENIGMA does not guarantee correct work of this extension
* on any other Magento edition except Magento community edition.
* dasENIGMA does not provide extension support in case of
* incorrect edition usage.
* =================================================================
*
* @category   Enigma
* @package    Enigma_LandingPages
* @version    1.0
* @copyright  Copyright (c) 2014 dasENIGMA. (http://codecanyon.net/user/dasEnigma/portfolio?ref=dasEnigma)
* @license    http://codecanyon.net/licenses/regular
*/
class Enigma_Xlanding_Block_Adminhtml_Page_Grid extends Mage_Adminhtml_Block_Widget_Grid{
	public function __construct(){
		parent::__construct();
		$this->setId('ruleGrid');
		$this->setDefaultSort('pos');
  	}

	protected function _prepareCollection(){
		$collection = Mage::getModel('elanding/page')->getCollection();
		$this->setCollection($collection);
		return parent::_prepareCollection();
  	}

	protected function _prepareColumns(){
		$hlp =  Mage::helper('elanding'); 
    
		$this->addColumn('pade_id', array(
			'header'    => $hlp->__('ID'),
			'align'     => 'right',
			'width'     => '50px',
			'index'     => 'page_id',
    	));
	    $this->addColumn('title', array(
			'header'    => $hlp->__('Title'),
	        'align'     => 'left',
	        'index'     => 'title',
		));
		$this->addColumn('identifier', array(
	    	'header'    => $hlp->__('URL Key'),
	        'align'     => 'left',
	        'index'     => 'identifier'
		));
		$this->addColumn('is_active', array(
            'header'    => Mage::helper('cms')->__('Status'),
            'index'     => 'is_active',
            'type'      => 'options',
            'options'   => $hlp->getAvailableStatuses()
        ));
		if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('store_id', array(
                'header'        => Mage::helper('cms')->__('Store View'),
                'index'         => 'store_id',
                'type'          => 'store',
                'store_all'     => true,
                'store_view'    => true,
                'sortable'      => false,
                'filter_condition_callback'
                                => array($this, '_filterStoreCondition'),
            ));
        } 
	    return parent::_prepareColumns();
  	}

	public function getRowUrl($row){
		return $this->getUrl('adminhtml/page/edit', array('id' => $row->getPageId()));
	}
  
 	protected function _afterLoadCollection(){
        $this->getCollection()->walk('afterLoad');
        parent::_afterLoadCollection();
    }

    protected function _filterStoreCondition($collection, $column){
        if (!$value = $column->getFilter()->getValue()) {
            return;
        }
        $this->getCollection()->addStoreFilter($value);
    }
    
    protected function _prepareMassaction(){
	    $this->setMassactionIdField('page_id');
	    $this->getMassactionBlock()->setFormFieldName('pages');
	    
	    $actions = array(
	        'massActivate' => 'Activate',
	        'massInactivate' => 'Inactivate',
	        'massDelete' => 'Delete',
	    );
	    foreach ($actions as $code => $label){
	        $this->getMassactionBlock()->addItem($code, array(
	             'label' => Mage::helper('elanding')->__($label),
	             'url' => $this->getUrl('adminhtml/page/' . $code),
	             'confirm' => ($code == 'massDelete' ? Mage::helper('elanding')->__('Are you sure?') : null),
	        ));        
	    }
	    return $this; 
	} 
}