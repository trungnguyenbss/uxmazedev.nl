<?php
/**
* dasENIGMA.
*
* NOTICE OF LICENSE
*
* This source file is subject to the EULA
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://codecanyon.net/licenses/regular
*
* =================================================================
*                 MAGENTO EDITION USAGE NOTICE
* =================================================================
* This package designed for Magento community edition
* dasENIGMA does not guarantee correct work of this extension
* on any other Magento edition except Magento community edition.
* dasENIGMA does not provide extension support in case of
* incorrect edition usage.
* =================================================================
*
* @category   Enigma
* @package    Enigma_LandingPages
* @version    1.0
* @copyright  Copyright (c) 2014 dasENIGMA. (http://codecanyon.net/user/dasEnigma/portfolio?ref=dasEnigma)
* @license    http://codecanyon.net/licenses/regular
*/
class MageWorx_SeoSuite_Block_Catalog_Product_List_Toolbar extends Mage_Catalog_Block_Product_List_Toolbar{
    public function getPagerUrl($params = array()){
        if ($identifier = Mage::app()->getRequest()->getParam('e_landing')) {
    		if (count($params) > 0) {
    			return $identifier . '?' . http_build_query($params);	
    		}
    		return $identifier; 
    	}		
        $urlParams = array();
        $urlParams['_current'] = true;
        $urlParams['_escape'] = true;
        $urlParams['_use_rewrite'] = true;
        $urlParams['_query'] = $params;
        return Mage::helper('seosuite')->getLayerFilterUrl($urlParams);
    }
    
    public function getPagerHtml(){
        if ($identifier = Mage::app()->getRequest()->getParam('e_landing')) {
            $alias = 'product_list_toolbar_pager';
            $oldPager = $this->getChild($alias);
            if ($oldPager instanceof Varien_Object){
                $newPager = $this->getLayout()->createBlock('elanding/catalog_pager')
                    ->setArea('frontend')
                    ->setTemplate($oldPager->getTemplate());                    
                $newPager->assign('_type', 'html')->assign('_section', 'body');                         
                $this->setChild($alias, $newPager);
            }        
            return parent::getPagerHtml();
        }
        return parent::getPagerHtml();
    }
}