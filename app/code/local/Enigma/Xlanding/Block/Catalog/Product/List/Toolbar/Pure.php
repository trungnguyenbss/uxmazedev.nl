<?php
/**
* dasENIGMA.
*
* NOTICE OF LICENSE
*
* This source file is subject to the EULA
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://codecanyon.net/licenses/regular
*
* =================================================================
*                 MAGENTO EDITION USAGE NOTICE
* =================================================================
* This package designed for Magento community edition
* dasENIGMA does not guarantee correct work of this extension
* on any other Magento edition except Magento community edition.
* dasENIGMA does not provide extension support in case of
* incorrect edition usage.
* =================================================================
*
* @category   Enigma
* @package    Enigma_LandingPages
* @version    1.0
* @copyright  Copyright (c) 2014 dasENIGMA. (http://codecanyon.net/user/dasEnigma/portfolio?ref=dasEnigma)
* @license    http://codecanyon.net/licenses/regular
*/
if ('true' == (string)Mage::getConfig()->getNode('modules/Enigma_Sorting/active')){
    class Enigma_Xlanding_Block_Catalog_Product_List_Toolbar_Pure extends Enigma_Sorting_Block_Catalog_Product_List_Toolbar {}
} else if ('true' == (string)Mage::getConfig()->getNode('modules/Enigma_Shopby/active')){
    class Enigma_Xlanding_Block_Catalog_Product_List_Toolbar_Pure extends Enigma_Shopby_Block_Catalog_Product_List_Toolbar {}
} else {
    class Enigma_Xlanding_Block_Catalog_Product_List_Toolbar_Pure extends Mage_Catalog_Block_Product_List_Toolbar {}
}