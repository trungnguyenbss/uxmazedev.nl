<?php
/**
* dasENIGMA.
*
* NOTICE OF LICENSE
*
* This source file is subject to the EULA
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://codecanyon.net/licenses/regular
*
* =================================================================
*                 MAGENTO EDITION USAGE NOTICE
* =================================================================
* This package designed for Magento community edition
* dasENIGMA does not guarantee correct work of this extension
* on any other Magento edition except Magento community edition.
* dasENIGMA does not provide extension support in case of
* incorrect edition usage.
* =================================================================
*
* @category   Enigma
* @package    Enigma_SpecialOccasionCoupons
* @version    1.0
* @copyright  Copyright (c) 2014 dasENIGMA. (http://codecanyon.net/user/dasEnigma/portfolio?ref=dasEnigma)
* @license    http://codecanyon.net/licenses/regular
*/
class Enigma_Base_Helper_Data extends Mage_Core_Helper_Abstract{
    public function isVersionLessThan($major = 1, $minor = 4){
        $curr = explode('.', Mage::getVersion());
        $need = func_get_args();
        foreach($need as $k => $v){
            if($curr[$k] != $v){
                return ($curr[$k] < $v);
			}
        }
        return false;
    }     
    public function isModuleActive($code){
        return ('true' == (string)Mage::getConfig()->getNode('modules/'.$code.'/active'));
    }     
}