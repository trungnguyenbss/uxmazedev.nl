<?php
/**
* dasENIGMA.
*
* NOTICE OF LICENSE
*
* This source file is subject to the EULA
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://codecanyon.net/licenses/regular
*
* =================================================================
*                 MAGENTO EDITION USAGE NOTICE
* =================================================================
* This package designed for Magento community edition
* dasENIGMA does not guarantee correct work of this extension
* on any other Magento edition except Magento community edition.
* dasENIGMA does not provide extension support in case of
* incorrect edition usage.
* =================================================================
*
* @category   Enigma
* @package    Enigma_SpecialOccasionCoupons
* @version    1.0
* @copyright  Copyright (c) 2014 dasENIGMA. (http://codecanyon.net/user/dasEnigma/portfolio?ref=dasEnigma)
* @license    http://codecanyon.net/licenses/regular
*/
class Enigma_Base_Block_Extensions extends Mage_Adminhtml_Block_System_Config_Form_Fieldset{
	protected $_dummyElement;
	protected $_fieldRenderer;
	protected $_values;

    public function render(Varien_Data_Form_Element_Abstract $element){
		$html = $this->_getHeaderHtml($element);
		$modules = array_keys((array)Mage::getConfig()->getNode('modules')->children());
		sort($modules);

        foreach($modules as $moduleName){
        	if(strstr($moduleName, 'Enigma_') === false) {
        		continue;
        	}			
			if($moduleName == 'Enigma_Base'){
				continue;
			}			
        	$html.= $this->_getFieldHtml($element, $moduleName);
        }
        $html .= $this->_getFooterHtml($element);
        return $html;
    }

    protected function _getFieldRenderer(){
    	if(empty($this->_fieldRenderer)){
    		$this->_fieldRenderer = Mage::getBlockSingleton('adminhtml/system_config_form_field');
    	}
    	return $this->_fieldRenderer;
    }

	protected function _getFieldHtml($fieldset, $moduleCode){
		$currentVer = Mage::getConfig()->getModuleConfig($moduleCode)->version;
		if (!$currentVer){
            return '';
		}
		$moduleName = substr($moduleCode, strpos($moduleCode, '_') + 1);
		$allExtensions = unserialize(Mage::app()->loadCache('enigmabase_extensions'));            
        $status = '<a  target="_blank"><img src="'.$this->getSkinUrl('images/enigmabase/ok.gif').'" title="'.$this->__("Installed").'"/></a>';
		if ($allExtensions && isset($allExtensions[$moduleCode])){
            $ext = $allExtensions[$moduleCode];            
            $url = $ext['url'];
            $name = $ext['display_name'];
            $lastVer = $ext['version'];
            $moduleName = '<a href="'.$url.'" target="_blank" title="'.$name.'">'.$name."</a>";
            if($this->_convertVersion($currentVer) < $this->_convertVersion($lastVer)){
                $status = '<a href="'.$url.'" target="_blank"><img src="'.$this->getSkinUrl('images/enigmabase/update.gif').'" alt="'.$this->__("Update available").'" title="'.$this->__("Update available").'"/></a>';
            }
        }
		$moduleName = $status . ' ' . $moduleName;
		$field = $fieldset->addField($moduleCode, 'label', array(
            'name'  => 'dummy',
            'label' => $moduleName,
            'value' => $currentVer,
		))->setRenderer($this->_getFieldRenderer());			
		return $field->toHtml();
    }
    
    protected function _convertVersion($v){
		$digits = @explode(".", $v);
		$version = 0;
		if(is_array($digits)){
			foreach ($digits as $k=>$v){
				$version += ($v * pow(10, max(0, (3-$k))));
			}			
		}
		return $version;
	}
}