<?php
/**
* dasENIGMA.
*
* NOTICE OF LICENSE
*
* This source file is subject to the EULA
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://codecanyon.net/licenses/regular
*
* =================================================================
*                 MAGENTO EDITION USAGE NOTICE
* =================================================================
* This package designed for Magento community edition
* dasENIGMA does not guarantee correct work of this extension
* on any other Magento edition except Magento community edition.
* dasENIGMA does not provide extension support in case of
* incorrect edition usage.
* =================================================================
*
* @category   Enigma
* @package    Enigma_SpecialOccasionCoupons
* @version    1.0
* @copyright  Copyright (c) 2014 dasENIGMA. (http://codecanyon.net/user/dasEnigma/portfolio?ref=dasEnigma)
* @license    http://codecanyon.net/licenses/regular
*/
class Enigma_Base_Model_Feed extends Mage_AdminNotification_Model_Feed{
    const XML_FREQUENCY_PATH = 'enigmabase/feed/check_frequency';
    const XML_LAST_UPDATE_PATH = 'enigmabase/feed/last_update';
    const XML_ITERESTS = 'enigmabase/feed/interests';    
    const URL_EXTENSIONS = 'http://dasenigma.com/feed-extensions.xml';
    const URL_NEWS = 'http://dasenigma.com/feed-news.xml';

	public static function check(){
		return Mage::getModel('enigmabase/feed')->checkUpdate();
	}
	
    public function checkUpdate(){
        if(($this->getFrequency() + $this->getLastUpdate()) > time()){
            return $this;
        }
        $this->setLastUpdate();        
        if(!extension_loaded('curl')){
            return $this;
        }
        $feedData   = array();
        $feedXml = $this->getFeedData();
        $wasInstalled = gmdate('Y-m-d H:i:s', Mage::getStoreConfig('enigmabase/feed/installed'));        
        if($feedXml && $feedXml->channel && $feedXml->channel->item){
            foreach($feedXml->channel->item as $item){
                $date = $this->getDate((string)$item->pubDate);
                if($date < $wasInstalled){
                    continue;
                }
                if(!$this->isInteresting($item)){
                    continue;
                }   
                $feedData[] = array(
                    'severity' => 3,
                    'date_added' => $this->getDate($date),
                    'title' => (string)$item->title,
                    'description' => (string)$item->description,
                    'url' => (string)$item->link,
                );
            }
            if($feedData){
                Mage::getModel('adminnotification/inbox')->parse($feedData);
            }
        }
        $this->_feedUrl = self::URL_EXTENSIONS;
        $feedData   = array();
        $feedXml = $this->getFeedData();
        if($feedXml && $feedXml->channel && $feedXml->channel->item){
            foreach($feedXml->channel->item as $item){
                $feedData[(string)$item->code] = array(
                    'name' => (string)$item->title,
                    'url' => (string)$item->link,
                    'version' => (string)$item->version,
                );
            }
            if($feedData){
                Mage::app()->saveCache(serialize($feedData), 'enigmabase_extensions');
            }
        }        
        return $this;
    }

    public function getFrequency(){
        return Mage::getStoreConfig(self::XML_FREQUENCY_PATH);
    }

    public function getLastUpdate(){
        return Mage::app()->loadCache('enigmabase_notifications_lastcheck');
    }
 
    public function setLastUpdate(){
        Mage::app()->saveCache(time(), 'enigmabase_notifications_lastcheck');
        return $this;
    }
    
    public function getFeedUrl(){
        if(is_null($this->_feedUrl)){
            $this->_feedUrl = self::URL_NEWS;
        }
        $query = '?s=' . urlencode(Mage::getStoreConfig('web/unsecure/base_url')); 
        return $this->_feedUrl  . $query;
    }
    
    protected function getInterests(){
		return Mage::getStoreConfig(self::XML_ITERESTS);
	}

	protected function isInteresting($item){
		$interests = @explode(',', $this->getInterests());
		$types     = @explode(':', (string)$item->type);
		$extenion  = (string)$item->extension;		
		$selfUpgrades = array_search(Enigma_Base_Model_Source_Updates_Type::TYPE_INSTALLED_UPDATE, $types);		
		foreach ($types as $type){
			if(array_search($type, $interests) !== false){
				return true;
			}			
			if($extenion && ($type == Enigma_Base_Model_Source_Updates_Type::TYPE_UPDATE_RELEASE) && $selfUpgrades){
                if($this->isExtensionInstalled($extenion)){
                	return true;
                }
			}
		}		
		return false;
	}

	protected function isExtensionInstalled($code){
		$modules = array_keys((array)Mage::getConfig()->getNode('modules')->children());
        foreach ($modules as $moduleName) {
        	if($moduleName == $code){
        		return true;
        	}
        }        
		return false;
	}    
}