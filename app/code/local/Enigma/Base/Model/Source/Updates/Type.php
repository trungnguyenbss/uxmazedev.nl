<?php
/**
* dasENIGMA.
*
* NOTICE OF LICENSE
*
* This source file is subject to the EULA
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://codecanyon.net/licenses/regular
*
* =================================================================
*                 MAGENTO EDITION USAGE NOTICE
* =================================================================
* This package designed for Magento community edition
* dasENIGMA does not guarantee correct work of this extension
* on any other Magento edition except Magento community edition.
* dasENIGMA does not provide extension support in case of
* incorrect edition usage.
* =================================================================
*
* @category   Enigma
* @package    Enigma_SpecialOccasionCoupons
* @version    1.0
* @copyright  Copyright (c) 2014 dasENIGMA. (http://codecanyon.net/user/dasEnigma/portfolio?ref=dasEnigma)
* @license    http://codecanyon.net/licenses/regular
*/
class Enigma_Base_Model_Source_Updates_Type extends Mage_Eav_Model_Entity_Attribute_Source_Abstract{
	const TYPE_PROMO = 'PROMO';
	const TYPE_NEW_RELEASE = 'NEW_RELEASE';
	const TYPE_UPDATE_RELEASE = 'UPDATE_RELEASE';
	const TYPE_INFO = 'INFO';
	const TYPE_INSTALLED_UPDATE = 'INSTALLED_UPDATE';
	
	public function toOptionArray(){
	    $hlp = Mage::helper('enigmabase');
		return array(
			array('value' => self::TYPE_INSTALLED_UPDATE, 'label' => $hlp->__('My extensions updates')),
			array('value' => self::TYPE_UPDATE_RELEASE,   'label' => $hlp->__('All extensions updates')),
			array('value' => self::TYPE_NEW_RELEASE,      'label' => $hlp->__('New Releases')),
			array('value' => self::TYPE_PROMO,            'label' => $hlp->__('Promotions/Discounts')),
			array('value' => self::TYPE_INFO,             'label' => $hlp->__('Other information'))
		);
	}
	
    public function getAllOptions(){
    	return $this->toOptionArray();
	}
	
	public function getLabel($value){
		$options = $this->toOptionArray();
		foreach($options as $v){
			if($v['value'] == $value){
				return $v['label'];
			}
		}
		return '';
	}
	
	public function getGridOptions(){
		$items = $this->getAllOptions();
		$out = array();
		foreach($items as $item){
			$out[$item['value']] = $item['label'];
		}
		return $out;
	}
}