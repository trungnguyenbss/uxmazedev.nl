<?php 
/**
 * Evince_Tumblr extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category   	Evince
 * @package		Evince_Tumblr
 * @copyright  	Copyright (c) 2014
 * @license		http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Tumblr Integration front contrller
 *
 * @category	Evince
 * @package		Evince_Tumblr
 * @author Evince Development
 */
class Evince_Tumblr_EvincetumblrController extends Mage_Core_Controller_Front_Action{
	/**
 	 * default action
 	 * @access public
 	 * @return void
 	 * @author Evince Development
 	 */
 	public function indexAction(){
		$this->loadLayout();
		$this->renderLayout();
	}
	/**
 	 * view tumblr integration action
 	 * @access public
 	 * @return void
 	 * @author Evince Development
 	 */
	public function viewAction(){
		Mage::register('current_tumblr_evincetumblr', $evincetumblr);
			$this->loadLayout();
			if ($root = $this->getLayout()->getBlock('root')) {
				$root->addBodyClass('tumblr-evincetumblr tumblr-evincetumblr');
			}
			$this->renderLayout();
	}
}