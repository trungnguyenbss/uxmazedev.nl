<?php 
/**
 * Evince_Tumblr extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category   	Evince
 * @package		Evince_Tumblr
 * @copyright  	Copyright (c) 2014
 * @license		http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Tumblr Integration list block
 *
 * @category	Evince
 * @package		Evince_Tumblr
 * @author Evince Development
 */
class Evince_Tumblr_Block_Evincetumblr_List extends Mage_Core_Block_Template{
	/**
	 * initialize
	 * @access public
	 * @return void
	 * @author Evince Development
	 */
 	public function __construct(){
		parent::__construct();
	}
	
	public function getEvincetumblrs()
	{
	
		$request = Mage::getStoreConfig('tumblr/evincetumblr/tumblr_api').'/api/read/json';
		$ci = curl_init($request);
		curl_setopt($ci, CURLOPT_RETURNTRANSFER, TRUE);
		$input = curl_exec($ci);
		
		// Tumblr JSON doesn't come in standard form, some str replace needed
		$input = str_replace('var tumblr_api_read = ','',$input);
		$input = str_replace(';','',$input);
		
		// parameter 'true' is necessary for output as PHP array
		$value = json_decode($input, true);
		$content =  $value['posts'];
		return $content;
	}
	/**
	 * prepare the layout
	 * @access protected
	 * @return Evince_Tumblr_Block_Evincetumblr_List
	 * @author Evince Development
	 */
	protected function _prepareLayout(){
		parent::_prepareLayout();
	}
}