<?php 
/**
 * Evince_Tumblr extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category   	Evince
 * @package		Evince_Tumblr
 * @copyright  	Copyright (c) 2014
 * @license		http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Tumblr Integration view block
 *
 * @category	Evince
 * @package		Evince_Tumblr
 * @author Evince Development
 */
class Evince_Tumblr_Block_Evincetumblr_View extends Mage_Core_Block_Template{
	/**
	 * get the current tumblr integration
	 * @access public
	 * @return mixed (Evince_Tumblr_Model_Evincetumblr|null)
	 * @author Evince Development
	 */
	public function getCurrentEvincetumblr(){
	
			$request = Mage::getStoreConfig('tumblr/evincetumblr/tumblr_api').'/api/read/json';
			$ci = curl_init($request);
			curl_setopt($ci, CURLOPT_RETURNTRANSFER, TRUE);
			$input = curl_exec($ci);
			
			$input = str_replace('var tumblr_api_read = ','',$input);
			$input = str_replace(';','',$input);
			
			
			$value = json_decode($input, true);
			$content =  $value['posts'];
			$item = count($content);
			
			$requested_id = $this->getRequest()->getParam('id');
			for ($i=0;$i<=$item;$i++) {
				if ($content[$i]['id'] == $requested_id) 
				{
						return $content[$i]['photo-caption'];
				}
			}
	}
} 