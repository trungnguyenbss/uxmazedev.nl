<?php 
/**
 * Evince_Tumblr extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category   	Evince
 * @package		Evince_Tumblr
 * @copyright  	Copyright (c) 2014
 * @license		http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Tumblr default helper
 *
 * @category	Evince
 * @package		Evince_Tumblr
 * @author Evince Development
 */
class Evince_Tumblr_Helper_Data extends Mage_Core_Helper_Abstract{
	/**
	 * get the url to the tumblr integration list page
	 * @access public
	 * @return string
	 * @author Evince Development
	 */
	public function getEvincetumblrsUrl(){
		return Mage::getUrl('tumblr/evincetumblr/index');
	}
}