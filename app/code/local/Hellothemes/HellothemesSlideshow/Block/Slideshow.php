<?php
/**
 * @version   2.0 
 * @author    Hellothemes
 * @copyright Copyright (C) 2010 - 2014 Hellothemes
 */

class Hellothemes_HellothemesSlideshow_Block_Slideshow extends Mage_Core_Block_Template
{
	
	protected function _beforeToHtml()
	{
		$config = Mage::getStoreConfig('hellothemesslideshow', Mage::app()->getStore()->getId());
		if (Mage::helper('hellothemesslideshow/data')->isSlideshowEnabled()) {

			$this->setTemplate('hellothemes/' . $config['config']['slider'] . '.phtml');
		}

		return $this;
	}

	public function _prepareLayout()
	{
		return parent::_prepareLayout();
	}

	public function getSlideshow()
	{

		if (!$this->hasData('hellothemesslideshow')) {
			$this->setData('hellothemesslideshow', Mage::registry('hellothemesslideshow'));
		}
		return $this->getData('hellothemesslideshow');

	}

	public function getSlides()
	{
		$config = Mage::getStoreConfig('hellothemesslideshow', Mage::app()->getStore()->getId());

		if ( $config['config']['slider'] == 'flexslider' ) {
			$model = Mage::getModel('hellothemesslideshow/hellothemesflex');
		} else {
			$model = Mage::getModel('hellothemesslideshow/hellothemesrevolution');
		}
		$slides = $model->getCollection()
			->addStoreFilter(Mage::app()->getStore())
			->addFieldToSelect('*')
			->addFieldToFilter('status', 1)
			->setOrder('sort_order', 'asc');
		return $slides;
	}

}