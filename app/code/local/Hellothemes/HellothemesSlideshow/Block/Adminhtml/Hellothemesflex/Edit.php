<?php
/**
 * @version   2.0 
 * @author    Hellothemes
 * @copyright Copyright (C) 2010 - 2014 Hellothemes
 */

class Hellothemes_HellothemesSlideshow_Block_Adminhtml_Hellothemesflex_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'hellothemesslideshow';
        $this->_controller = 'adminhtml_hellothemesflex';
        
        $this->_updateButton('save', 'label', Mage::helper('hellothemesslideshow')->__('Save Slide'));
        $this->_updateButton('delete', 'label', Mage::helper('hellothemesslideshow')->__('Delete Slide'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('hellothemesflex_data') && Mage::registry('hellothemesflex_data')->getId() ) {
            return Mage::helper('hellothemesslideshow')->__("Edit Slide");
        } else {
            return Mage::helper('hellothemesslideshow')->__('Add Slide');
        }
    }
}