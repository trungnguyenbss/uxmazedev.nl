<?php
/**
 * @version   2.0 
 * @author    Hellothemes
 * @copyright Copyright (C) 2010 - 2014 Hellothemes
 */

class Hellothemes_HellothemesSlideshow_Block_Adminhtml_Hellothemesrevolution_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('hellothemesrevolution_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('hellothemesslideshow')->__('Revolution Slide Information'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('hellothemesslideshow')->__('Revolution Slide Information'),
          'title'     => Mage::helper('hellothemesslideshow')->__('Revolution Slide Information'),
          'content'   => $this->getLayout()->createBlock('hellothemesslideshow/adminhtml_hellothemesrevolution_edit_tab_form')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
}