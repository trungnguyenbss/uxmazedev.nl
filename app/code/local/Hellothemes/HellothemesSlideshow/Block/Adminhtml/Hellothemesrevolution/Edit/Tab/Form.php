<?php
/**
 * @version   2.0 
 * @author    Hellothemes
 * @copyright Copyright (C) 2010 - 2014 Hellothemes
 */

/* Page = Revolution Slider slides->Edit slide */

class Hellothemes_HellothemesSlideshow_Block_Adminhtml_Hellothemesrevolution_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm()
	{

		$model = Mage::registry('hellothemesslideshow_hellothemesrevolution');

		$form = new Varien_Data_Form();
		$this->setForm($form);
		$fieldset = $form->addFieldset('hellothemesrevolution_form', array('legend' => Mage::helper('hellothemesslideshow')->__('Revolution Slide information')));

		$fieldset->addField('store_id', 'multiselect', array(
			'name' => 'stores[]',
			'label' => Mage::helper('hellothemesslideshow')->__('Store View'),
			'title' => Mage::helper('hellothemesslideshow')->__('Store View'),
			'required' => true,
			'values' => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
		));

		$fieldset->addField('transition', 'select', array(
			'label' => Mage::helper('hellothemesslideshow')->__('Transition'),
			'name' => 'transition',
			'values' => array(
				array(
					'value' => 'boxslide',
					'label' => Mage::helper('hellothemesslideshow')->__('boxslide'),
				),
				array(
					'value' => 'boxfade',
					'label' => Mage::helper('hellothemesslideshow')->__('boxfade'),
				),
				array(
					'value' => 'slotzoom-horizontal',
					'label' => Mage::helper('hellothemesslideshow')->__('slotzoom-horizontal'),
				),
				array(
					'value' => 'slotslide-horizontal',
					'label' => Mage::helper('hellothemesslideshow')->__('slotslide-horizontal'),
				),
				array(
					'value' => 'slotfade-horizontal',
					'label' => Mage::helper('hellothemesslideshow')->__('slotfade-horizontal'),
				),
				array(
					'value' => 'slotzoom-vertical',
					'label' => Mage::helper('hellothemesslideshow')->__('slotzoom-vertical'),
				),
				array(
					'value' => 'slotslide-vertical',
					'label' => Mage::helper('hellothemesslideshow')->__('slotslide-vertical'),
				),
				array(
					'value' => 'slotfade-vertical',
					'label' => Mage::helper('hellothemesslideshow')->__('slotfade-vertical'),
				),
				array(
					'value' => 'curtain-1',
					'label' => Mage::helper('hellothemesslideshow')->__('curtain-1'),
				),
				array(
					'value' => 'curtain-2',
					'label' => Mage::helper('hellothemesslideshow')->__('curtain-2'),
				),
				array(
					'value' => 'curtain-3',
					'label' => Mage::helper('hellothemesslideshow')->__('curtain-3'),
				),
				array(
					'value' => 'slideleft',
					'label' => Mage::helper('hellothemesslideshow')->__('slideleft'),
				),
				array(
					'value' => 'slideright',
					'label' => Mage::helper('hellothemesslideshow')->__('slideright'),
				),
				array(
					'value' => 'slideup',
					'label' => Mage::helper('hellothemesslideshow')->__('slideup'),
				),
				array(
					'value' => 'slidedown',
					'label' => Mage::helper('hellothemesslideshow')->__('slidedown'),
				),
				array(
					'value' => 'fade',
					'label' => Mage::helper('hellothemesslideshow')->__('fade'),
				),
				array(
					'value' => 'random',
					'label' => Mage::helper('hellothemesslideshow')->__('random'),
				),
				array(
					'value' => 'slidehorizontal',
					'label' => Mage::helper('hellothemesslideshow')->__('slidehorizontal'),
				),
				array(
					'value' => 'slidevertical',
					'label' => Mage::helper('hellothemesslideshow')->__('slidevertical'),
				),
				array(
					'value' => 'papercut',
					'label' => Mage::helper('hellothemesslideshow')->__('papercut'),
				),
				array(
					'value' => 'flyin',
					'label' => Mage::helper('hellothemesslideshow')->__('flyin'),
				),
				array(
					'value' => 'turnoff',
					'label' => Mage::helper('hellothemesslideshow')->__('turnoff'),
				),
				array(
					'value' => 'cube',
					'label' => Mage::helper('hellothemesslideshow')->__('cube'),
				),
				array(
					'value' => '3dcurtain-vertical',
					'label' => Mage::helper('hellothemesslideshow')->__('3dcurtain-vertical'),
				),
				array(
					'value' => '3dcurtain-horizontal',
					'label' => Mage::helper('hellothemesslideshow')->__('3dcurtain-horizontal'),
				),
			),
			'note' => 'The appearance transition of this slide',
		));

		$fieldset->addField('masterspeed', 'text', array(
			'label' => Mage::helper('hellothemesslideshow')->__('Masterspeed'),
			'required' => false,
			'name' => 'masterspeed',
			'note' => 'Set the Speed of the Slide Transition. Default 300, min:100 max:2000.'
		));
		$fieldset->addField('slotamount', 'text', array(
			'label' => Mage::helper('hellothemesslideshow')->__('Slotamount'),
			'required' => false,
			'name' => 'slotamount',
			'note' => 'The number of slots or boxes the slide is divided into. If you use boxfade, over 7 slots can be juggy.'
		));
		$fieldset->addField('link', 'text', array(
			'label' => Mage::helper('hellothemesslideshow')->__('Slide Link'),
			'required' => false,
			'name' => 'link',
		));

		$data = array();
		$out = '';
		if (Mage::getSingleton('adminhtml/session')->getHellothemesslideshowData()) {
			$data = Mage::getSingleton('adminhtml/session')->getHellothemesslideshowData();
		} elseif (Mage::registry('hellothemesslideshow_data')) {
			$data = Mage::registry('hellothemesslideshow_data')->getData();
		}

		if (!empty($data['image'])) {
			$url = Mage::getBaseUrl('media') . $data['image'];
			$out = '<br/><center><a href="' . $url . '" target="_blank" id="imageurl">';
			$out .= "<img src=" . $url . " width='150px' />";
			$out .= '</a></center>';
		}

		$fieldset->addField('image', 'file', array(
			'label' => Mage::helper('hellothemesslideshow')->__('Image'),
			'required' => false,
			'name' => 'image',
			'note' => $out,
		));

		$out = '';
		if (!empty($data['thumb'])) {
			$url = Mage::getBaseUrl('media') . $data['thumb'];
			$out = '<br/><center><a href="' . $url . '" target="_blank" id="imageurl">';
			$out .= "<img src=" . $url . " width='150px' />";
			$out .= '</a></center>';
		}

		$fieldset->addField('thumb', 'file', array(
			'label' => Mage::helper('hellothemesslideshow')->__('Slide thumb'),
			'required' => false,
			'name' => 'thumb',
			'note' => 'An Alternative Source for thumbs. If not defined a copy of the background image will be used in resized form. ' . $out,
		));

		$fieldset->addField('text', 'textarea', array(
			'label'     => Mage::helper('hellothemesslideshow')->__('Slide Content'),
			'required'  => false,
			'name'      => 'text',
		));

		$fieldset->addField('status', 'select', array(
			'label' => Mage::helper('hellothemesslideshow')->__('Status'),
			'name' => 'status',
			'values' => array(
				array(
					'value' => 1,
					'label' => Mage::helper('hellothemesslideshow')->__('Enabled'),
				),
				array(
					'value' => 2,
					'label' => Mage::helper('hellothemesslideshow')->__('Disabled'),
				),
			),
		));

		$fieldset->addField('sort_order', 'text', array(
			'label' => Mage::helper('hellothemesslideshow')->__('Sort Order'),
			'required' => false,
			'name' => 'sort_order',
		));

		if (Mage::getSingleton('adminhtml/session')->getHellothemesslideshowData()) {
			$form->setValues(Mage::getSingleton('adminhtml/session')->getHellothemesslideshowData());
			Mage::getSingleton('adminhtml/session')->getHellothemesslideshowData(null);
		} elseif (Mage::registry('hellothemesslideshow_data')) {
			$form->setValues(Mage::registry('hellothemesslideshow_data')->getData());
		}
		return parent::_prepareForm();
	}
}