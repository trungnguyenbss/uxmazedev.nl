<?php
/**
 * @version   2.0 
 * @author    Hellothemes
 * @copyright Copyright (C) 2010 - 2014 Hellothemes
 */

class Hellothemes_HellothemesSlideshow_Block_Adminhtml_Hellothemesflex extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct()
	{
		$this->_controller = 'adminhtml_hellothemesflex';
		$this->_blockGroup = 'hellothemesslideshow';
		$this->_headerText = Mage::helper('hellothemesslideshow')->__('Flexslider Slides Manager');
		$this->_addButtonLabel = Mage::helper('hellothemesslideshow')->__('Add Slide');
		parent::__construct();
	}
}