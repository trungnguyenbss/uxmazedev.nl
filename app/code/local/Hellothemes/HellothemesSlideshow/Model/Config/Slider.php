<?php
/**
 * @version   2.0 
 * @author    Hellothemes
 * @copyright Copyright (C) 2010 - 2014 Hellothemes
 */

class Hellothemes_HellothemesSlideshow_Model_Config_Slider
{
    public function toOptionArray()
    {
	    $options = array();
	    $options[] = array(
            'value' => 'flexslider',
            'label' => 'Flexslider',
        );
        //Comentar esto para q no aparezca la opcion revolution en el framework base (tienen q comprar la ext rev.slider para q aparezca la sig opcion):
        $options[] = array(
            'value' => 'revolution',
            'label' => 'Revolution slider',
        );

        return $options;
    }

}
