<?php
/**
 * @version   2.0 
 * @author    Hellothemes
 * @copyright Copyright (C) 2010 - 2014 Hellothemes
 */

class Hellothemes_HellothemesSlideshow_Model_Config_Yesno
{
    public function toOptionArray()
    {
	    $options = array();
	    $options[] = array(
            'value' => 'true',
            'label' => 'Yes',
        );
        $options[] = array(
            'value' => 'false',
            'label' => 'No',
        );

        return $options;
    }

}
