<?php
/**
 * @version   2.0 
 * @author    Hellothemes
 * @copyright Copyright (C) 2010 - 2014 Hellothemes
 */

class Hellothemes_HellothemesSlideshow_Model_Config_Revolution_Navbar
{
    public function toOptionArray()
    {
	    $options = array();
        $options[] = array(
            'value' => 'none',
            'label' => 'none',
        );
	    $options[] = array(
            'value' => 'bullet',
            'label' => 'bullet',
        );
        $options[] = array(
            'value' => 'thumb',
            'label' => 'thumb',
        );
        $options[] = array(
            'value' => 'both',
            'label' => 'both',
        );

        return $options;
    }

}
