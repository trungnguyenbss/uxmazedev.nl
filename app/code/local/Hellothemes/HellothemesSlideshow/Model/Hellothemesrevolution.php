<?php
/**
 * @version   2.0 
 * @author    Hellothemes
 * @copyright Copyright (C) 2010 - 2014 Hellothemes
 */

class Hellothemes_HellothemesSlideshow_Model_Hellothemesrevolution extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('hellothemesslideshow/hellothemesrevolution');
    }

}