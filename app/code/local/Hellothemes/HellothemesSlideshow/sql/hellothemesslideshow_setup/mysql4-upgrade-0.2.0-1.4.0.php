<?php
/**
 * @version   1.0 12.0.2012
 * @author    hellothemes http://www.hellothemes.com <mail@hellothemes.com>
 * @copyright Copyright (C) 2010 - 2012 hellothemes
 */

$installer = $this;
/* @var $installer Mage_Catalog_Model_Resource_Eav_Mysql4_Setup */
$installer->startSetup();
/* flexslider */
$installer->setConfigData('hellothemesslideshow/config/slider', 'flexslider');
$installer->setConfigData('hellothemesslideshow/flexslider/height', '');
$installer->setConfigData('hellothemesslideshow/flexslider/animation', 'slide');
$installer->setConfigData('hellothemesslideshow/flexslider/slideshow', 'true');
$installer->setConfigData('hellothemesslideshow/flexslider/animation_loop', 'true');
$installer->setConfigData('hellothemesslideshow/flexslider/mousewheel', 'false');
$installer->setConfigData('hellothemesslideshow/flexslider/smoothheight', 'false');
$installer->setConfigData('hellothemesslideshow/flexslider/slideshow_speed', '7000');
$installer->setConfigData('hellothemesslideshow/flexslider/animation_speed', '400');
$installer->setConfigData('hellothemesslideshow/flexslider/control_nav', 'false');
$installer->setConfigData('hellothemesslideshow/flexslider/direction_nav', 'true');
$installer->setConfigData('hellothemesslideshow/flexslider/timeline', 'true');
/* revolution slider */
$installer->setConfigData('hellothemesslideshow/revolutionslider/delay', '9000');
$installer->setConfigData('hellothemesslideshow/revolutionslider/startheight', '460');
$installer->setConfigData('hellothemesslideshow/revolutionslider/startwidth', '1170');
$installer->setConfigData('hellothemesslideshow/revolutionslider/hideThumbs', '200');
$installer->setConfigData('hellothemesslideshow/revolutionslider/navigationType', 'bullet');
$installer->setConfigData('hellothemesslideshow/revolutionslider/navigationArrows', 'verticalcentered');
$installer->setConfigData('hellothemesslideshow/revolutionslider/navigationStyle', 'round');
$installer->setConfigData('hellothemesslideshow/revolutionslider/touchenabled', 'on');
$installer->setConfigData('hellothemesslideshow/revolutionslider/navOffsetHorizontal', '0');
$installer->setConfigData('hellothemesslideshow/revolutionslider/navOffsetVertical', '-14');
$installer->setConfigData('hellothemesslideshow/revolutionslider/onHoverStop', 'on');
$installer->setConfigData('hellothemesslideshow/revolutionslider/thumbWidth', '100');
$installer->setConfigData('hellothemesslideshow/revolutionslider/thumbHeight', '50');
$installer->setConfigData('hellothemesslideshow/revolutionslider/thumbAmount', '5');
$installer->setConfigData('hellothemesslideshow/revolutionslider/hideCaptionAtLimit', '');
$installer->setConfigData('hellothemesslideshow/revolutionslider/hideAllCaptionAtLilmit', '');
$installer->setConfigData('hellothemesslideshow/revolutionslider/hideSliderAtLimit', '');
$installer->setConfigData('hellothemesslideshow/revolutionslider/stopAtSlide', '-1');
$installer->setConfigData('hellothemesslideshow/revolutionslider/stopAfterLoops', '-1');
$installer->setConfigData('hellothemesslideshow/revolutionslider/timeline', '1');
$installer->setConfigData('hellothemesslideshow/revolutionslider/timeline_position', 'top');

$installer->run("

DROP TABLE IF EXISTS `{$this->getTable('hellothemesslideshow/revolution_slides')}`;
CREATE TABLE `{$this->getTable('hellothemesslideshow/revolution_slides')}` (
  `slide_id` int(11) unsigned NOT NULL auto_increment,
  `transition` text NOT NULL default '',
  `masterspeed` text NOT NULL default '',
  `slotamount` text NOT NULL default '',
  `link` varchar(255) NOT NULL default '',
  `thumb` varchar(255) NOT NULL default '',
  `image` varchar(255) NOT NULL default '',
  `text` text NOT NULL default '',
  `status` smallint(6) NOT NULL default '0',
  `sort_order` smallint(6) NOT NULL default '0',
  `created_time` datetime NULL,
  `update_time` datetime NULL,
  PRIMARY KEY (`slide_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `{$this->getTable('hellothemesslideshow/revolution_slides')}` (`slide_id`, `transition`, `masterspeed`, `slotamount`, `link`, `thumb`, `image`, `text`, `status`, `sort_order`, `created_time`, `update_time`) VALUES
	(1, 'papercut', '460', '1', '', '', 'hellothemes/hellothemesslideshow/revolution/nonino1_1.jpg', '', 1, 2, '2013-01-05 16:16:16', '2013-01-05 16:16:16'),
	(2, 'fade', '300', '1', '', '', 'hellothemes/hellothemesslideshow/revolution/nonino2_1.jpg', '', 1, 1, '2013-01-05 16:17:06', '2013-01-05 16:17:06'),
	(3, 'slideleft', '300', '1', '', '', 'hellothemes/hellothemesslideshow/revolution/nonino1_1.jpg', '', 1, 2, '2013-01-05 16:18:06', '2013-01-05 16:18:06'),
	(4, 'slidedown', '300', '7', '', '', 'hellothemes/hellothemesslideshow/revolution/nonino2_1.jpg', '', 1, 3, '2013-01-05 16:21:20', '2013-01-05 16:21:20');

");

/**
 * Drop 'slides_store' table
 */
$conn = $installer->getConnection();
$conn->dropTable($installer->getTable('hellothemesslideshow/revolution_slides_store'));

/**
 * Create table for stores
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('hellothemesslideshow/revolution_slides_store'))
    ->addColumn('slide_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
    'nullable'  => false,
    'primary'   => true,
), 'Slide ID')
    ->addColumn('store_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
    'unsigned'  => true,
    'nullable'  => false,
    'primary'   => true,
), 'Store ID')
    ->addIndex($installer->getIdxName('hellothemesslideshow/revolution_slides_store', array('store_id')),
    array('store_id'))
    ->addForeignKey($installer->getFkName('hellothemesslideshow/revolution_slides_store', 'slide_id', 'hellothemesslideshow/revolution_slides', 'slide_id'),
    'slide_id', $installer->getTable('hellothemesslideshow/revolution_slides'), 'slide_id',
    Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->addForeignKey($installer->getFkName('hellothemesslideshow/revolution_slides_store', 'store_id', 'core/store', 'store_id'),
    'store_id', $installer->getTable('core/store'), 'store_id',
    Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('Slide To Store Linkage Table');
$installer->getConnection()->createTable($table);

/**
 * Assign 'all store views' to existing slides
 */
$installer->run("INSERT INTO {$this->getTable('hellothemesslideshow/revolution_slides_store')} (`slide_id`, `store_id`) SELECT `slide_id`, 0 FROM {$this->getTable('hellothemesslideshow/revolution_slides')};");

$installer->endSetup();

/**
 * add slide data
 */
$data = array(
    1 => 
    '
<div class="caption sft"  data-speed="1000" data-x="150" data-y="100" data-start="1000" data-easing="easeOutBack"><img src="{{media url="hellothemes/hellothemesslideshow/revolution/nonino.png"}}" alt="Nonino"></div>
<div class="caption lfr"  data-speed="1000" data-x="210" data-y="160" data-start="2000" data-easing="easeInElastic"><img src="{{media url="hellothemes/hellothemesslideshow/revolution/lineabig.png"}}"></div>
<div class="caption lfl"  data-speed="1000" data-x="227" data-y="168" data-start="3000" data-easing="easeInElastic"><img src="{{media url="hellothemes/hellothemesslideshow/revolution/lineasmall.png"}}"></div>
<div class="caption sfb hello_small_text_light"  data-x="50" data-y="200" data-start="4000" data-easing="easeOutBack">Lorem Ipsum is simply dummy text of the printing and typesetting. Lorem Ipsum has</br> been the standard dummy text of the industry since the 1500s, when an unknown</br> printer (T. N. person who is engaged in press) unknown took a galley of type and mixed</br> so that managed to make a specimen textbook</div>',
    '
<div class="caption fade"  data-speed="500" data-x="102" data-y="-12" data-start="1000" data-easing="easeOutBack"><img src="{{media url="hellothemes/hellothemesslideshow/revolution/img1.png"}}"></div>
<div class="caption sft"  data-speed="500" data-x="150" data-y="100" data-start="2500" data-easing="easeOutBack"><img src="{{media url="hellothemes/hellothemesslideshow/revolution/nonino.png"}}" alt="Nonino"></div>
<div class="caption lfr"  data-speed="1000" data-x="210" data-y="160" data-start="3000" data-easing="easeInElastic"><img src="{{media url="hellothemes/hellothemesslideshow/revolution/lineabig.png"}}"></div>
<div class="caption lfl"  data-speed="1000" data-x="227" data-y="168" data-start="4000" data-easing="easeInElastic"><img src="{{media url="hellothemes/hellothemesslideshow/revolution/lineasmall.png"}}"></div>
<div class="caption randomrotate hello_small_text_light"  data-x="50" data-y="200" data-start="5000" data-easing="easeOutBack">Lorem Ipsum is simply dummy text of the printing and typesetting. Lorem Ipsum has</br> been the standard dummy text of the industry since the 1500s, when an unknown</br> printer (T. N. person who is engaged in press) unknown took a galley of type and mixed</br> so that managed to make a specimen textbook</div>',
);

$model = Mage::getModel('hellothemesslideshow/hellothemesrevolution');
foreach ( $data as $k => $v ) {
    $model->load($k)
        ->setText($v)
        ->save();
}