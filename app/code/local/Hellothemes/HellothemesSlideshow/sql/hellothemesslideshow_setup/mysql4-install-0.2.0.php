<?php
/**
 * @version   1.0 
 * @author    Hellothemes
 * @copyright Copyright (C) 2010 - 2014 Hellothemes
 */

$installer = $this;
$installer->startSetup();
$installer->run("

DROP TABLE IF EXISTS `{$this->getTable('hellothemesslideshow/flex_slides')}`;
CREATE TABLE `{$this->getTable('hellothemesslideshow/flex_slides')}` (
  `slide_id` int(11) unsigned NOT NULL auto_increment,
  `slide_align` ENUM('left', 'right', 'center'),
  `slide_title` text NOT NULL default '',
  `slide_text` text NOT NULL default '',
  `slide_button` text NOT NULL default '',
  `slide_width` varchar(8) NOT NULL default '',
  `slide_link` varchar(255) NOT NULL default '',
  `image` varchar(255) NOT NULL default '',
  `small_image` varchar(255) NOT NULL default '',
  `status` smallint(6) NOT NULL default '0',
  `sort_order` smallint(6) NOT NULL default '0',
  `created_time` datetime NULL,
  `update_time` datetime NULL,
  PRIMARY KEY (`slide_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `{$this->getTable('hellothemesslideshow/flex_slides')}` (`slide_id`, `slide_align`, `slide_title`, `slide_text`, `slide_button`, `slide_link`, `image`, `status`, `sort_order`, `created_time`, `update_time`) VALUES (1, 'left', 'Lorem Ipsum<br/> Dolor sit Amen', '\'60s-inspired bangles, pendants, and head pieces<br/>from Nicole Richie\'s boho-chic line', 'learn more', 'http://www.hellothemes.com', 'hellothemes/hellothemesslideshow/flex/nonino1_1.jpg', 1, 10, NOW(), NOW() );
INSERT INTO `{$this->getTable('hellothemesslideshow/flex_slides')}` (`slide_id`, `slide_align`, `slide_title`, `slide_text`, `slide_button`, `slide_link`, `image`, `status`, `sort_order`, `created_time`, `update_time`) VALUES (3, 'left', '', '', '', '', 'hellothemes/hellothemesslideshow/flex/nonino2_1.jpg', 1, 30, NOW(), NOW() );

");

/**
 * Drop 'slides_store' table
 */
$conn = $installer->getConnection();
$conn->dropTable($installer->getTable('hellothemesslideshow/flex_slides_store'));

/**
 * Create table for stores
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('hellothemesslideshow/flex_slides_store'))
    ->addColumn('slide_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
    'nullable'  => false,
    'primary'   => true,
), 'Slide ID')
    ->addColumn('store_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
    'unsigned'  => true,
    'nullable'  => false,
    'primary'   => true,
), 'Store ID')
    ->addIndex($installer->getIdxName('hellothemesslideshow/flex_slides_store', array('store_id')),
    array('store_id'))
    ->addForeignKey($installer->getFkName('hellothemesslideshow/flex_slides_store', 'slide_id', 'hellothemesslideshow/flex_slides', 'slide_id'),
    'slide_id', $installer->getTable('hellothemesslideshow/flex_slides'), 'slide_id',
    Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->addForeignKey($installer->getFkName('hellothemesslideshow/flex_slides_store', 'store_id', 'core/store', 'store_id'),
    'store_id', $installer->getTable('core/store'), 'store_id',
    Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('Slide To Store Linkage Table');
$installer->getConnection()->createTable($table);

/**
 * Assign 'all store views' to existing slides
 */
$installer->run("INSERT INTO {$this->getTable('hellothemesslideshow/flex_slides_store')} (`slide_id`, `store_id`) SELECT `slide_id`, 0 FROM {$this->getTable('hellothemesslideshow/flex_slides')};");
$installer->endSetup();