<?php
/**
 * @version   1.0 19.07.2013
 * @author    Hellothemes http://www.hellothemes.com <support@hellothemes.com>
 * @copyright Copyright (C) 2009 - 2013 Hellothemes
 */

class Hellothemes_HellothemesSettings_Model_Product {

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
	public function toArray() {
		/* Read XML with list of Hellothemes products */

		$feedURL	= 'http://www.hellothemes.com/helloframework/products.xml';
		$ch	= curl_init($feedURL);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$data = curl_exec($ch);
		$sxml = simplexml_load_string($data);
		$product[] = array('label'=>Mage::helper('adminhtml')->__("Select theme"));
		
		foreach($sxml->children() as $child){
		$product[]	= array('value' => $child->id, 'label'=>Mage::helper('adminhtml')->__($child->name));
		}
		
		return $product;
	}

}