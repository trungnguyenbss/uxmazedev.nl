<?php
/**
 * @version   1.1 09.08.2013
 * @author    Hellothemes http://www.hellothemes.com <support@hellothemes.com>
 * @copyright Copyright (C) 2009 - 2013 Hellothemes
 */

class Hellothemes_HellothemesSettings_Model_Config_Image extends Mage_Adminhtml_Model_System_Config_Backend_Image {

	protected function _beforeSave() {
   
        $value = $this->getValue();
		
        if ($_FILES['groups']['tmp_name'][$this->getGroupId()]['fields'][$this->getField()]['value'] != "") {

            $uploadDir = $this->_getUploadDir();

            try {
                $file             = array();
                $tmpName          = $_FILES['groups']['tmp_name'];
                $file['tmp_name'] = $tmpName[$this->getGroupId()]['fields'][$this->getField()]['value'];
                $name             = $_FILES['groups']['name'];
                $file['name']     = $name[$this->getGroupId()]['fields'][$this->getField()]['value'];
                $uploader         = new Mage_Core_Model_File_Uploader($file);
                $uploader->setAllowedExtensions($this->_getAllowedExtensions());
                $uploader->setAllowRenameFiles(TRUE);
                $uploader->addValidateCallback('size', $this, 'validateMaxSize');
                //here I added new name same as scopeId
                $ext    = explode('.', $file['name']);
                $ext    = array_pop($ext);
                $result = $uploader->save($uploadDir, $this->getScopeId() . '.' . $ext);

            } catch (Exception $e) {
                Mage::throwException($e->getMessage());
                return $this;
            }

            $filename = $result['file'];
            if ($filename) {
                if ($this->_addWhetherScopeInfo()) {
                    $filename = $this->_prependScopeInfo($filename);
                }
                $this->setValue($filename);
            }
        } else {
			if (empty($value['delete'])) {
                $this->setValue($value);
            } else {
				$this->setValue('');
            }
        }

        return $this;
		
    }

}