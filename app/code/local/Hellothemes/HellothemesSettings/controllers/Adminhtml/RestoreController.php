<?php
/**
 * @version   1.0 19.07.2013
 * @author    Hellothemes http://www.hellothemes.com <support@hellothemes.com>
 * @copyright Copyright (C) 2009 - 2013 Hellothemes
 */

class Hellothemes_HellothemesSettings_Adminhtml_RestoreController extends Mage_Adminhtml_Controller_Action {

    protected $_stores;
    protected $_clear;

    protected function _isAllowed(){
        return Mage::getSingleton('admin/session')
            ->isAllowed('hellothemes/hellothemes/restore');
    }

    protected function _initAction(){
        $this->loadLayout()
            ->_setActiveMenu('hellothemes/hellothemes/restore')
            ->_addBreadcrumb(Mage::helper('hellothemessettings')->__('HelloFramework Restoration'), Mage::helper('hellothemessettings')->__('HelloFramework Restoration'));

        return $this;
    }

    public function indexAction(){
        $this->_initAction();
        $this->_title($this->__('Hellothemes'))
            ->_title($this->__('HelloFramework Restore'));

        $this->_addContent($this->getLayout()->createBlock('hellothemessettings/adminhtml_restore_edit'));
        $block = $this->getLayout()->createBlock('core/text', 'restore-desc')
                ->setText('<a href="https://www.hellothemes.com"><img src="http://www.hellothemes.com/helloframework/banners/hellothemes.jpg" border=0 alt="HelloThemes" /></a>');
        $this->_addLeft($block);

        $this->renderLayout();
    }

    public function restoreAction(){
	    try {
			$this->action	= "restore";
			/* Call Hello API */
			$url	= "http://www.hellothemes.com/helloframework/api.php?method=auth&username={$this->getRequest()->getParam('username', 0)}&password={$this->getRequest()->getParam('password', 0)}&product={$this->getRequest()->getParam('product', 0)}";
			$ch	= curl_init($url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$data	= curl_exec($ch);

			$json	= json_decode($data,true);
			
			if(!$json["status"]) { Mage::getSingleton('adminhtml/session')->addError(Mage::helper('hellothemessettings')->__($json["msg"])); $this->getResponse()->setRedirect($this->getUrl("*/*/")); }
			eval(Mage::helper('hellothemessettings/data')->decryptcode($json["code"],$this->getRequest()->getParam('username', 0)));
			if(isset($this->flag))
				if(!$this->flag) Mage::getSingleton('adminhtml/session')->addError(Mage::helper('hellothemessettings')>__('There is a problem in your server settings. Please contact us: support@hellothemes.com'));
        }
        catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('hellothemessettings')->__('An error occurred while restoring theme settings.'));
        }
        $this->getResponse()->setRedirect($this->getUrl("*/*/"));
    }
	
	/**
     * create/update cms page/static block
     */
    protected function execute($item, $model) {
        $cms = array();
        foreach ( $item as $p ) { // creating the array
            $cms[$p->getName()] = (string)$p;
	        if ( $p->getName() == 'stores' ) {
		        $cms[$p->getName()] = array();
		        foreach ( $p as $store ) {
			        $cms[$p->getName()][] = (string)$store;
		        }
	        }
        }
	    foreach ($this->stores as $store) {
			$created = Mage::getModel($model)->getCollection()
				->addStoreFilter($store)
				->addFieldToFilter('identifier', array( 'eq' => $cms['identifier'] ))
				->load();
			if (count($created)) {
				foreach ($created as $_page) {
					$_page->delete();
				}
			}
		}
		// Replace Store
		$cms["stores"]	= $this->stores;
		
	    Mage::getModel($model)->setData($cms)->save(); // save in db
    }

    private  function saveconfigdata($items, $path){
		if ($this->_clear) {
			foreach ($items as $item) {
				if ( count($item->children()) > 0 ) {
					$this->saveconfigdata($item->children(), $path.'/'.$item->getName());
				} else {
					// Remove Core Config Data with the same Store
					foreach ($this->stores as $store) {
						Mage::getConfig()->deleteConfig($path.'/'.$item->getName(), 'stores', $store);
					}
					// Create default config data
					foreach ($this->stores as $store) {
						$scope = ($store ? 'stores' : 'default');
						//echo $path . '/' . $item->getName() . " - " . (string)$item . " - " . $scope . " - " . $store . "<br>";
						Mage::getConfig()->saveConfig($path.'/'.$item->getName(), (string)$item, $scope, $store);
					}
				}
			}
		}
    }

}