<?php
/**
 * @version   1.0 19.07.2013
 * @author    Hellothemes http://www.hellothemes.com <support@hellothemes.com>
 * @copyright Copyright (C) 2009 - 2013 Hellothemes
 */

class Hellothemes_HellothemesSettings_Block_Adminhtml_System_Config_Form_Field_Sortable extends Mage_Adminhtml_Block_System_Config_Form_Field {

    /**
     * Override field method to add js
     *
     * @param Varien_Data_Form_Element_Abstract $element
     * @return String
     */
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element){

        // Get the default HTML for this option
        $html = parent::_getElementHtml($element);
		
		if ( !Mage::registry('sortable') ) {
			$html .= '
				<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
				<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

				<script>
					jQuery(function() {
						// hide order input
						jQuery("#row_hellothemessettings_blocks_block_left_nav_order").hide();
						jQuery("#row_hellothemessettings_blocks_block_layered_nav_order").hide();
						jQuery("#row_hellothemessettings_blocks_block_viewed_order").hide();
						jQuery("#row_hellothemessettings_blocks_block_tags_order").hide();
						jQuery("#row_hellothemessettings_blocks_block_compare_order").hide();
						jQuery("#row_hellothemessettings_blocks_block_poll_order").hide();
						jQuery("#hellothemessettings_blocks_block_sortable").hide();
						jQuery("#hellothemessettings_blocks_block_sortable_inherit").hide();
						jQuery(\'[for="hellothemessettings_blocks_block_sortable_inherit"]\').hide();
						
						jQuery( "#sortable" ).disableSelection();
						jQuery("#sortable").sortable({
							connectWith: "#sortable",
							cursor: "move",
							update: function(event, ui) {
								var result = jQuery(this).sortable("toArray", {attribute: "value"});
								var length = result.length;
								element = null;
								var sort = [];
								for (var i = 0; i < length; i++) {
									element = result[i];
									switch(element){
										case 1:
											jQuery("#hellothemessettings_blocks_block_layered_nav_order").val(i);
											sort.push("1");
											break;
										case 2:
											jQuery("#hellothemessettings_blocks_block_viewed_order").val(i);
											sort.push("2");
											break;
										case 3:
											jQuery("#hellothemessettings_blocks_block_tags_order").val(i);
											sort.push("3");
											break;
										case 4:
											jQuery("#hellothemessettings_blocks_block_compare_order").val(i);
											sort.push("4");
											break;
										case 5:
											jQuery("#hellothemessettings_blocks_block_poll_order").val(i);
											sort.push("5");
											break;
										default:
											jQuery("#hellothemessettings_blocks_block_left_nav_order").val(i);
											sort.push("0");
											break;
									}
								}
								jQuery("#hellothemessettings_blocks_block_sortable").val(sort);
							}
						});
					});
				</script>
				';
			Mage::register('sortable', 1);
        }
		
		$html .= '
		<div class="sortable-content">
			<span>Sidebar Order:</span>
			<ul id="sortable" class="myList">
				<li value="0" class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>Sidebar Menu</li>
				<li value="1" class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>Layered Navigation</li>
				<li value="2" class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>Viewed Products</li>
				<li value="3" class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>Tags</li>
				<li value="4" class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>Compare Products</li>
				<li value="5" class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>Poll</li>
			</ul>
		</div>
		<script>
			// Get your list items
			var items = jQuery("#sortable").find("li");
			var order = jQuery("#hellothemessettings_blocks_block_sortable").val();
			
			// Map the existing items to their new positions        
			var orderedItems = jQuery.map(order, function(value) {
				return items.get(value);
			});
			// Clear the old list items and insert the newly ordered ones
			jQuery("#sortable").empty().html(orderedItems);
		</script>
		';
		
        return $html;
    }

}