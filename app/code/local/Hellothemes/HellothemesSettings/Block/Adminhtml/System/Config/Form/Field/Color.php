<?php
/**
 * @version   1.0 19.07.2013
 * @author    Hellothemes http://www.hellothemes.com <support@hellothemes.com>
 * @copyright Copyright (C) 2009 - 2013 Hellothemes
 */

class Hellothemes_HellothemesSettings_Block_Adminhtml_System_Config_Form_Field_Color extends Mage_Adminhtml_Block_System_Config_Form_Field {
    
	protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element) {
        $html	= parent::_getElementHtml($element);
		
		$store	= Mage::app()->getRequest()->getParam('store');	
		if($store) {
			$c = Mage::getModel('core/store')->getCollection()->addFieldToFilter('code', $store);        
			$item = $c->getFirstItem();
			// Toma el nombre del theme
			$theme	= Mage::getStoreConfig("hellothemessettings/appearance/theme",$item->getStoreId());
		}
		
        if ( !Mage::registry('mColorPicker') ) {
            $html .= '
                <script type="text/javascript" src="'.$this->getJsUrl('hellothemes/jquery-1.8.2.min.js').'"></script>
                <script type="text/javascript" src="'.$this->getJsUrl('hellothemes/mColorPicker.min.js').'"></script>
                <script type="text/javascript" src="'.$this->getJsUrl('hellothemes/fancybox/jquery.fancybox-1.3.4.pack.js').'"></script>
				<link type="text/css" href="'.$this->getJsUrl('hellothemes/fancybox/jquery.fancybox-1.3.4.css').'" rel="stylesheet">
				
                <script type="text/javascript">
					jQuery.noConflict();
					jQuery.fn.mColorPicker.init.replace = true;
					jQuery.fn.mColorPicker.init.enhancedSwatches = true;
					jQuery.fn.mColorPicker.init.allowTransparency = true;
					jQuery.fn.mColorPicker.init.showLogo = false;
					jQuery.fn.mColorPicker.defaults.imageFolder = "'.$this->getJsUrl('hellothemes/mColorPicker/').'";
					jQuery(document).ready(function(){
						jQuery(".fancy a").fancybox();
					});
                </script>
                ';
			/* Show Framework Settings for each theme */
			$cssurl	= Mage::getBaseUrl('skin') . "frontend/default/{$theme}/css/framework_settings.css";
			$html .= "<link type='text/css' href='{$cssurl}' rel='stylesheet'>";
            Mage::register('mColorPicker', 1);
        }
		$html .= '
        <script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery("#'.$element->getHtmlId().'").width("100px").attr("data-hex", true).mColorPicker();
		})
        </script>
		<div class="fancy">
			<a href="http://'.$_SERVER['SERVER_NAME']."/media/hellothemes/".$theme."/framework/".$element->getHtmlId().'.jpg" rel="group">
				<img src="'.$this->getJsUrl('hellothemes/fancybox/hellothemes_rescue.png').'" title="HelloThemes to the rescue" width="20px" />
			</a>
		</div>
        ';
		
        return $html;
    }
}