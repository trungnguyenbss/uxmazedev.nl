<?php
/**
 * @version   1.0 19.07.2013
 * @author    Hellothemes http://www.hellothemes.com <support@hellothemes.com>
 * @copyright Copyright (C) 2009 - 2013 Hellothemes
 */

class Hellothemes_HellothemesSettings_Block_Adminhtml_Restore_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {
    
	public function __construct() {
        parent::__construct();

        $this->_blockGroup = 'hellothemessettings';
        $this->_controller = 'adminhtml_restore';
        $this->_updateButton('save', 'label', Mage::helper('hellothemessettings')->__('Restore'));
        $this->_removeButton('delete');
        $this->_removeButton('back');
    }

    public function getHeaderText() {
        return Mage::helper('hellothemessettings')->__('HelloFramework Restoration');
    }
}
