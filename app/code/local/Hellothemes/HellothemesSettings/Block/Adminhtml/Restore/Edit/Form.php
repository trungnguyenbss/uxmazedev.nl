<?php
/**
 * @version   1.0 19.07.2013
 * @author    Hellothemes http://www.hellothemes.com <support@hellothemes.com>
 * @copyright Copyright (C) 2009 - 2013 Hellothemes
 */

class Hellothemes_HellothemesSettings_Block_Adminhtml_Restore_Edit_Form extends Mage_Adminhtml_Block_Widget_Form {
    
	protected function _prepareForm(){
        $isElementDisabled = false;
        $form = new Varien_Data_Form();

        $fieldset = $form->addFieldset('base_fieldset', array('legend'=>Mage::helper('adminhtml')->__('HelloFramework Default')));

        /**
         * Check is single store mode
         */
        if (!Mage::app()->isSingleStoreMode()) {
            $fieldset->addField('store_id', 'multiselect', array(
                'name'      => 'stores[]',
                'label'     => Mage::helper('cms')->__('Store View'),
                'title'     => Mage::helper('cms')->__('Store View'),
                'required'  => true,
                'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
                'disabled'  => $isElementDisabled
            ));
        } else {
            $fieldset->addField('store_id', 'hidden', array(
                'name'      => 'stores[]',
                'value'     => 0
            ));
        }

        $fieldset->addField('clear_scope', 'checkbox', array(
                'name'  => 'clear_scope',
                'value' => 1,
                'label' => Mage::helper('hellothemessettings')->__('Set Framework configuration to default'),
                'title' => Mage::helper('hellothemessettings')->__('Set Framework configuration to default'),
            )
        );

	    $fieldset->addField('setup_cms', 'checkbox', array(
            'label' => Mage::helper('hellothemessettings')->__('Set Cms Pages & Blocks to default'),
		    'note' => Mage::helper('hellothemessettings')->__('ATTENTION: All changes to cms pages/blocks will be lost'),
            'required' => false,
            'name' => 'setup_cms',
            'value' => 1,
        ));
		
		$fieldset->addField('username', 'text', array(
			'label' => Mage::helper('hellothemessettings')->__('Username (HelloThemes)'),
			'name'		=> 'username',
			'value'		=> '',
			'required'	=> true,
		));
		
		$fieldset->addField('password', 'password', array(
			'label' => Mage::helper('hellothemessettings')->__('Password (HelloThemes)'),
			'name'		=> 'password',
			'value'		=> '',
			'required'	=> true,
		));
		
		$fieldset->addField('product', 'select', array(
			'label' 	=> Mage::helper('hellothemessettings')->__('Product'),
			'name'		=> 'product',
			'values'    => Mage::getModel('hellothemessettings/product')->toArray(),
			'required'	=> true,
		));

        $form->setAction($this->getUrl('*/*/restore'));
        $form->setMethod('post');
        $form->setUseContainer(true);
        $form->setId('edit_form');

        $this->setForm($form);

        return parent::_prepareForm();
    }
}
