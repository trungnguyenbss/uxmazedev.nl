<?php
/**
 * @version   1.0 19.07.2013
 * @author    Hellothemes http://www.hellothemes.com <support@hellothemes.com>
 * @copyright Copyright (C) 2009 - 2013 Hellothemes
 */

class Hellothemes_HellothemesSettings_Helper_Data extends Mage_Core_Helper_Abstract
{

    protected function _loadProduct(Mage_Catalog_Model_Product $product){
        $product->load($product->getId());
    }

    public function getNewLabel(Mage_Catalog_Model_Product $product){
        if ( 'Mage_Catalog_Model_Product' != get_class($product) )
            return;
        $this->_loadProduct($product);
		$new	= $this->newlabel($product);
        if ( $new[0] ) {
            $html = $this->__('New');
        }
		return $html;
	}
	public function getSaleLabel(Mage_Catalog_Model_Product $product){
		if ( 'Mage_Catalog_Model_Product' != get_class($product) )
            return;
        $this->_loadProduct($product);
		$special	= $this->speciallabel($product);
        if ( $special[0] ) {
            $html .= $this->__('Sale To:') . $special[1];
        }
		return $html;        
    }	
	
	// Sale label for helloLoja and helloNevada:
	public function getSaleLabelOnly(Mage_Catalog_Model_Product $product){
		if ( 'Mage_Catalog_Model_Product' != get_class($product) )
            return;
        $this->_loadProduct($product);
		$special	= $this->speciallabel($product);
        if ( $special[0] ) {
            $html .= $this->__('Sale');
        }
		return $html;        
    }

    // "To:" label only, for helloNevada display
	public function getToOnly(Mage_Catalog_Model_Product $product){
		if ( 'Mage_Catalog_Model_Product' != get_class($product) )
            return;
        $this->_loadProduct($product);
		$special	= $this->speciallabel($product);
        if ( $special[0] ) {
            $html .= $this->__('To: ')  . $special[1];
        }
		return $html;        
    }
    protected function newlabel($product) {
		// New Label
		$new_label = false;
		if ($product->getData('news_from_date') == true){
			$date	= date("Y-m-d H:i:s");
			$date = strtotime($date);
			$from = strtotime($product->getData('news_from_date'));
			$to = strtotime($product->getData('news_to_date'));
			if ($date >= $from && $date <= $to) {
				$from = date("d F", strtotime($product->getData('news_from_date')));
				$new_label = array(true,$from);
			}
		}
        return $new_label;
    }

    protected function speciallabel($product) {
		// Special Label
		$special_label = false;
		$date	= date("Y-m-d H:i:s");
		$date = strtotime($date);
		$from = strtotime($product->getData('special_from_date'));
		$to = strtotime($product->getData('special_to_date'));
		if ($date >= $from && $date <= $to) {
			$to = date("d F", strtotime($product->getData('special_to_date')));
			$special_label = array(true,$to);
		}
        return $special_label;
    }
	
	public function decryptcode($string,$key) {
		$result = '';
		$string = base64_decode($string);
		for($i=0; $i<strlen($string); $i++) {
			$char = substr($string, $i, 1);
			$keychar = substr($key, ($i % strlen($key))-1, 1);
			$char = chr(ord($char)-ord($keychar));
			$result.=$char;
		}
		return $result;
	}

	public function getFeaturedProducts($categoryId) {
		$products = Mage::getSingleton('catalog/category')->load($categoryId)
		    ->getProductCollection()
		    ->addAttributeToSelect('*');

		return $products;
	}

}